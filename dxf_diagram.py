import ezdxf
import random  # needed for random placing points


def get_random_point():
    """Returns random x, y coordinates."""
    x = random.randint(-100, 100)
    y = random.randint(-100, 100)
    return x, y


# Create a new drawing in the DXF format of AutoCAD 2010
doc = ezdxf.new('R2010')

# Create a block with the name 'FLAG'
flag = doc.blocks.new(name='FLAG')
flag2 = doc.blocks.new(name='FLAG2')
PH = doc.blocks.new(name='PH')
PH.add_line((0, 0), (0.17, 0))
# PH.add_line((0, 0.4), (0, 0.6))
# PH.add_line((0, 0.8), (0, 1))
# PH.add_line((0, 1.2), (0, 1.4))
points=[]

# Add DXF entities to the block 'FLAG'.
# The default base point (= insertion point) of the block is (0, 0).
points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
flag.add_line((0, 1), (0.3, 0.6))
flag.add_line((0.5, 1), (0.3, 0.6))
flag.add_line((0.25, -0.2), (0.25, 0))
flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
flag2.add_line((0, 1), (0.3, 0.6))
flag2.add_line((0.5, 1), (0.3, 0.6))
flag2.add_line((0.25, -0.2), (0.25, 0))
flag2.add_line((0.25, -0.6), (0.25, -0.5))
flag2.add_line((0.25, -1), (0.25, -0.9))
# flag2.add_line((0.25, -1.4), (0.25, -1.3))
# flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
msp = doc.modelspace()

# Get 50 random placing points.
placing_points = [get_random_point() for _ in range(50)]
x_count = 0
y_count = 0
for point in range(5):
    x_count += 0.78
    y_count += 1
    point=(x_count,0)
    print("points",points)
    # Every flag has a different scaling and a rotation of -15 deg.
    random_scale = 0.5 + random.random() * 2.0
    # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
    msp.add_blockref('FLAG', point, dxfattribs={
        'xscale': 0.2,
        'yscale': 0.3,
        'rotation': 0
    })
    # point=(x_count+2,0.3)
    # msp.add_blockref('PH', point, dxfattribs={
    #     'xscale': 0.2,
    #     'yscale': 0.1,
    #     'rotation': 0
    # })
    point=(x_count,0.6)
    msp.add_blockref('FLAG2', point, dxfattribs={
        'xscale': 0.2,
        'yscale': 0.3,
        'rotation': 0
    })
    x_count += 0.05
    point=(x_count,-0.06)
    msp.add_blockref('PH', point, dxfattribs={
        'xscale': 5,
        'yscale': 0.1,
        'rotation': 0
    })

# Save the drawing.
doc.saveas("blockref_tutorial.dxf")