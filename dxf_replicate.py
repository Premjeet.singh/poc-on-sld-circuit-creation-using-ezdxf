# import ezdxf
# from ezdxf.addons import Importer
#
# source_dxf = ezdxf.readfile("pv.dxf")
#
# if 'b_test' not in source_dxf.blocks:
#     print("Block 'b_test' not defined.")
#     exit()
#
# target_dxf = ezdxf.readfile("pv.dxf")
#
# importer = Importer(source_dxf, target_dxf)
# importer.import_block('b_test')
# importer.finalize()
#
# msp = target_dxf.modelspace()
# msp.add_blockref('b_test', insert=(10, 10))
# target_dxf.saveas("blockref_tutorials.dxf")
# from ezdxf.addons import iterdxf
#
# doc = iterdxf.opendxf('pvs.dxf')
# line_exporter = doc.export('new_line.dxf')
# text_exporter = doc.export('new_text.dxf')
# polyline_exporter = doc.export('new_polyline.dxf')
# dxf_test = ezdxf.readfile("pvs.dxf")
# msp_test = dxf_test.modelspace()
# msp_test.add_blockref('dxf_test', (2, 1), dxfattribs={
#     'xscale': 1,
#     'yscale': 1,
#     'rotation': 0
# })
# dxf_test.saveas("blockref_tutorialssss.dxf")
# try:
#     for entity in doc.modelspace():
#         if entity.dxftype() == 'LINE':
#             # msp_test.add_blockref('entity', (2, 1), dxfattribs={
#             #     'xscale': 1,
#             #     'yscale': 1,
#             #     'rotation': 0
#             # })
#             line_exporter.write(entity)
#         elif entity.dxftype() == 'TEXT':
#             text_exporter.write(entity)
#         elif entity.dxftype() == 'POLYLINE':
#             polyline_exporter.write(entity)
#     # dxf_test.saveas("blockref_tutorialssss.dxf")
# finally:
#     line_exporter.close()
#     text_exporter.close()
#     polyline_exporter.close()
#     doc.close()
#%%
import ezdxf
from ezdxf.addons import Importer
import random
import os
def get_random_point():
    """Returns random x, y coordinates."""
    x = random.randint(-100, 100)
    y = random.randint(-100, 100)
    return x, y

dir2save = 'C:/Users/dell/Downloads'
fc1name = os.path.join(dir2save,'grid.dxf')
pvname = os.path.join(dir2save,'panels.dxf')

#doc = ezdxf.new('R2010')
fclist = [fc1name,pvname]
labels = [ "pv"]
newdoc = ezdxf.new('R2010')
for fn,lab in zip(fclist,labels):
    doc = ezdxf.readfile(fn)
    print("doc",doc)
    importer = Importer(doc,newdoc)
    importer.import_modelspace()

    tblock = newdoc.blocks.new(name=lab)
    ents = doc.modelspace().query('LINE')
    print("panels",doc.linetypes)
    doc.linetypes('DASHED')
    for entity in doc.modelspace():
        print(entity.dxftype())
    #         if entity.dxftype() == 'LINE':
    #             # msp_test.add_blockref('entity', (2, 1), dxfattribs={
    #             #     'xscale': 1,
    #             #     'yscale': 1,
    #             #     'rotation': 0
    #             # })

    importer.import_entities(ents,tblock)

    msp = newdoc.modelspace()
    placing_points = [get_random_point() for _ in range(5)]

    for point in placing_points:
        # Every flag has a different scaling and a rotation of -15 deg.
        random_scale = 0.5 + random.random() * 2.0
        # Add a block reference to the block named 'Comb' at the coordinates 'point'.
        # if lab =='pv':
        #     scaler = 50
        # else:
        #     scaler=
        scaler=50
        msp.add_blockref(lab, point, dxfattribs={
            'xscale': random_scale*scaler,
            'yscale': random_scale*scaler,
            'rotation': 90*random_scale
        })


# Now read PV



# flag = doc.blocks.new(name='FLAG')

# # Add DXF entities to the block 'FLAG'.
# # The default base point (= insertion point) of the block is (0, 0).
# flag.add_lwpolyline([(0, 0), (0, 5), (4, 3), (0, 3)])  # the flag symbol as 2D polyline
# flag.add_circle((0, 0), .4, dxfattribs={'color': 2})  # mark the base point with a circle
importer.finalize()

newdoc.saveas("blockref_read_replicate.dxf")
