import json  # for writing, saving and opening JSON files
# import matplotlib
import ezdxf
from ezdxf.addons import Importer
import random
import os
# d = schem.Drawing()
# d.push()
l1=2
global max_y_limit
# max_y_limit=0


def draw_diagram(end_points,data_raw,frequency,counts,end_point,blockref_inverter,LINEDOWN,count_list,MPPTin,maximum,diff,total_mpptin,total_dict_length,Mp,doc,msp):
    global max_y_limit,max_x_limit,maximum_y,difference,mpp,total_length
    max_y_limit=0
    max_x_limit=total_mpptin
    maximum_y=maximum
    difference=diff
    mpp=Mp
    total_length=total_dict_length
    if MPPTin <=20:
        length = 25
        BB_x = 10
        inverter_x = 9
        in_y = 1
        name_y = 0.69
        in_y = 0
        out_y = 0
        max_y = 0
        used_y = 0
    if MPPTin>20 and MPPTin <=60:
        length = 32
        BB_x = 20
        inverter_x = 15
        name_y = 0.69
        in_y = 0
        out_y = 0
        max_y = 0
        used_y = 0


    if maximum <= 20:
        x = -3
        lengths = 5
        l5 = 4
        x_increase=0.2
        # BB_x=10
        # inverter_x=9
    elif maximum > 20 and maximum <= 60:
        x = -3
        lengths = 13
        l5 = 12
        x_increase=0.2
        # BB_x=20
        # inverter_x=16
    elif maximum>60 and maximum<=150:
        x = -3
        lengths = 29.9
        l5 = 29
        x_increase=0.2
    else:
        x=-3
        lengths=(26*diff)+29.9
        l5=lengths-1
        x_increase=0.2
    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
        y = 0
        end_points = end_points
        dict_lenght = 0
        p = 0.6
        count = 0
        dict_lenght = 0
        for end in end_point:
            dict_lenght += 1
            first_key = end_point[0]
            last_key = end_point[len(end_point) - 1]
            if len(data_raw) >= 1 and len(end_point) >= 1 and (
                    count != len(data_raw)):
                line_down = msp.add_blockref(LINEDOWN, end.dxf.insert, dxfattribs={
                        'layer': 'MyLines',
                        'xscale': 2,
                        'yscale': -lengths,
                        'rotation': 0
                    })
                if dict_lenght != 1:
                    arrow = msp.add_blockref('ARROW', end.dxf.insert+ [-0.8, -0.5], dxfattribs={
                        'layer': 'MyLines',
                        'xscale': 0.3,
                        'yscale': 0.2,
                        'rotation': 0
                    })

                point_y1 = -lengths - 0.05
                point_x1 = line_down.dxf.insert[0]
                point_y1 = point_y1
                point_x2 = blockref_inverter.dxf.insert[0] - x
                point_y2 = point_y1
                final_point = (point_x2, point_y2)
                msp.add_line((point_x1, point_y1), (final_point[0], final_point[1]))
                line_downss = msp.add_blockref('LINE_DOWN', final_point, dxfattribs={
                    'xscale': 2,
                    'yscale': -0.1 - p,
                    'rotation': 0
                })

                # x_pos=5
                # if dict_lenght == len(end_point):
                #     x_pos = 5
                #     inverter_label.add_attdef(inverter_name, (inverter_x, name_y),
                #                               dxfattribs={'height': 0.3, 'color': 3,'rotation':-360})
                #     inverter_label.add_attdef(inverter_in, (BB_x, 1.2 - in_y),
                #                               dxfattribs={'height': 0.2, 'color': 3,'rotation':-360})
                #     inverter_label.add_attdef(inverter_out, (BB_x, -0.39 - out_y),
                #                               dxfattribs={'height': 0.2, 'color': 3,'rotation':-360})
                #     inverter_label.add_attdef(inverter_max_inputs, (inverter_x, 0.3 - max_y),
                #                               dxfattribs={'height': 0.3, 'color': 3,'rotation':360})
                #     inverter_label.add_attdef(inverter_used_inputs, (inverter_x, -0.2 - used_y),
                #                               dxfattribs={'height': 0.3, 'color': 3,'rotation':360})
                #     values = {
                #         'inverter_max_inputs': MaxInputs + '10',
                #         'inverter_used_inputs': UsedInputs + str(MPPTin),
                #         'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                #         'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                #         'inverter_name': InverterNr,
                #         'XPOS': x_pos,
                #         'YPOS': 0
                #     }
                #     # point = (0.35, 0.9)
                #     # point_labels = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - 0.8,
                #     #                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - 6)
                #     # labels = msp.add_blockref(inverter_labels, point_labels, dxfattribs={
                #     #     'xscale': 0.2,
                #     #     'yscale': 0.3,
                #     #     'rotation': 0
                #     # })
                #     line_downss.add_auto_attribs(values)
            lengths -= 0.2
            l5 -= 0.2
            x -= 0.2
            y -= 1
            p += x_increase
    else:
        count = 0
        y = 0
        end_points = end_points
        dict_lenght = 0
        p = 0.6
        max_y_limit = 0
        max_x_limit=total_mpptin
        maximum_y=maximum
        difference=diff
        mpp = Mp
        total_length = total_dict_length
        if maximum <= 20:
            x = -3
            lengths = 5
            l5 = 4
            x_increase=0.2
        elif maximum > 20 and maximum <= 60:
            x = -3
            lengths = 13
            l5 = 12
            x_increase=0.2
        elif maximum > 60 and maximum <= 150:
            x = -3
            lengths = 29.9
            l5 = 29
            x_increase=0.2
        else:
            x = -3
            lengths = (26 * diff) + 29.9
            l5 = lengths - 1
            x_increase=0.2
        for end in end_points:
            dict_lenght += 1
            first_key = list(end_points.values())[0]
            last_key = list(end_points.values())[len(end_points) - 1]
            if len(data_raw) >=1  and len(end_point) >= 1 and (
                    count != len(data_raw)):
                line_down = msp.add_blockref(LINEDOWN, end_points[end].dxf.insert, dxfattribs={
                        'layer': 'MyLines',
                        'xscale': 2,
                        'yscale': -lengths,
                        'rotation': 0
                    })
                point_y1 = -lengths - 0.05
                    # lines = msp.query('LINE[layer=="MyLiness"]')

                    # print("**********layer",line.insert)
                    # msp.add_line(line_down.dxf.insert,line_downss.dxf.insert)

                #     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
                #     # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
                # else:
                #     line_down = msp.add_blockref(LINEDOWN, end_points[end].dxf.insert, dxfattribs={
                #         'xscale': 2,
                #         'yscale': -l5,
                #         'rotation': 0
                #     })
                #     point_y1 = -l5
                    # d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
                # if dict_lenght !=len(end_points):
                point_x1 = line_down.dxf.insert[0]
                point_y1 = point_y1
                point_x2 = blockref_inverter.dxf.insert[0] - x
                point_y2 = point_y1
                final_point = (point_x2, point_y2)
                msp.add_line((point_x1, point_y1), (final_point[0], final_point[1]))
                line_downss = msp.add_blockref('LINE_DOWN', final_point, dxfattribs={
                    'xscale': 2,
                    'yscale': -0.1 - p,
                    'rotation': 0
                })
            lengths -= 0.2
            l5 -= 0.2
            x -= 0.2
            y -= 1
            p += x_increase