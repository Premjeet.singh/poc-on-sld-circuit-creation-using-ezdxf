from typing import List
import entity


class PV(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.PolyLine(
                points=[entity.Point(-1, 10), entity.Point(0, 10), entity.Point(0, 7.5), entity.Point(-1, 7.5), entity.Point(-1, 10)],
                closed=True),
            entity.Line(entity.Point(-1, 10), entity.Point(-0.5, 9.5)),
            entity.Line(entity.Point(0, 10), entity.Point(-0.5, 9.5)),
        ]


class Dashed_Line(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            # entity.PolyLine(
            #     points=[entity.Point(-1, 10), entity.Point(0, 10), entity.Point(0, 7.5), entity.Point(-1, 7.5), entity.Point(-1, 10)],
            #     closed=True),
            entity.Line(entity.Point(0, 0), entity.Point(0, 0.2)),
            entity.Line(entity.Point(0, 0.4), entity.Point(0, 0.6)),
            entity.Line(entity.Point(0, 0.8), entity.Point(0, 1)),
            entity.Line(entity.Point(0, 1.2), entity.Point(0, 1.4)),
            # entity.Line(entity.Point(0, 4), entity.Point(0, 4.5)),
            # entity.Line(entity.Point(0, 5), entity.Point(0, 5.5)),
        ]


# class ITERM(entity.CodedSymbol):
#     def __init__(self, *args, left=True, right=True, label=None, **kwargs):
#         self.left = left
#         self.right = right
#         self.label = label
#         super().__init__(*args, **kwargs)
#
#     def generate(self):
#         base: List[entity.Entity] = [entity.Circle(entity.Point(10, 0), 5)]
#
#         if self.left:
#             base.append(entity.Line(entity.Point(0, 0), entity.Point(5, 0)))
#
#         if self.right:
#             base.append(entity.Line(entity.Point(15, 0), entity.Point(20, 0)))
#
#         return base

        # if self.label is not None:
        #     self.add_text(label, (10, -10),
        #                   height=10, alignment='MIDDLE_CENTER')
global x
x=0


class circuit(entity.CodedSymbol):
    min_pole = 1
    max_pole = 4

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        # for i in  range(2):
        #     print("hihi")
        #     if i == 0:
        #         x = 0
        #     else:
        #         x += 2
            return [
                PV(),
                Dashed_Line(yoff=1),
            # entity.Line(entity.Point(-0.5, 7), entity.Point(-0.5, 7.5)),
                PV().translate(yoff=3.9),
                PV().translate(xoff=5),
                Dashed_Line(yoff=1),
                # entity.Line(entity.Point(-0.5, 7), entity.Point(-0.5, 7.5)),
                PV().translate(yoff=3.9),
            # Dashed_Line(yoff=3.9)
            # entity.Line(entity.Point(-0.5, 7), entity.Point(-0.5, 7.5)),
            # entity.PolyLine(
            #     points=[entity.Point(-1, 7), entity.Point(0, 7), entity.Point(0, 4.5), entity.Point(-1, 4.5),
            #             entity.Point(-1, 7)],
            #     closed=True),
            # entity.Line(entity.Point(-1, 7), entity.Point(-0.5, 6.5)),
            # entity.Line(entity.Point(0, 7), entity.Point(-0.5, 6.5)),
        ]