# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
# import matplotlib
import os
from os import listdir
import sys
import subprocess  # open pdf after plot
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
# from matplotlib.backends.backend_ps import FigureCanvasPS
from sldcircuitsdxf.utilities import draw_diagram,draw_diagram_brazilian
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
import ezdxf
from ezdxf.addons import Importer
from ezdxf.math import Vec3
import random
import os


# matplotlib.use('Agg')  # Set the backend here, to save as PDF
res = {}
res={"String Config":{1: [[9, 9], [10, 10]],

 2: [[9], [10]],

 }}


def Ec_dxf(res, pvdata, invdata, dxf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        bb_label = doc.blocks.new(name='BB_labels')
        inverter_label = doc.blocks.new(name='inverter_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag.add_attdef('NAME', (-1.2, 2.3), dxfattribs={'height': 0.3, 'color': 3})
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 2), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 3})

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 1.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls
            MPPTin = key
            counts = count_parallelss
            maximum=max
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]

                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 0.78 + point[0]
                            y_count = 1 + point[1]
                            x_count += 0.02
                            y_count += 1
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 4.9,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 3,
                                    'yscale': -0.2,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 0.78 + point[0]
                            y_count = 1 + point[1]
                            x_count += 0.02
                            y_count += 1
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 4.9,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 3,
                                    'yscale': -0.2,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    for data in data_raw:
                        temp += 1
                        temp6 += 1

                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            x_count = 0.78 + point[0]
                            y_count =1 + point[1]
                            x_count += 0.02
                            y_count += 1

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME3':  str(frequency[data]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 5,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            if frequency[data] == 1:
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 10.95
                        w_l = 4
                        w_ls = 4
                        inv_y=6
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 15.5
                        w_l = 7.5
                        w_ls = 4
                        inv_y=14
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 60.5
                        w_l = 4
                        w_ls = 14.5
                        inv_y = 30.9
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 60.5
                        inv_y = (diff * 26) + 30.9
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                    if MPPTin <=20:
                        l = 8
                        # BB_x=48
                        # inverter_x=9
                        inv_length=1.5
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 15
                        # BB_x=60
                        # inverter_x = 18
                        inv_length = 3
                        # inv_y=14

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 29
                        BB_x=69
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12
                    else:
                        l = (diff * 10) + 29
                        BB_x=(diff*13.5)+69
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12

                    inverter.add_attdef('inverter_name', (-0.2, 0.4), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0.3), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, 0), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.3), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, -0.089), dxfattribs={'height': 0.09, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    global blockref_inverter;
                    point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                             list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                    blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                        'xscale': l,
                        'yscale': 1.5,
                        'rotation': 0
                    })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,doc, msp)

            else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 10.95
                        w_l = 4
                        w_ls = 4
                        inv_y=6
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 15.5
                        w_l = 7.5
                        w_ls = 4
                        inv_y=14
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 60.5
                        w_l = 4
                        w_ls = 14.5
                        inv_y=30.9
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 60.5
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        inv_y = (diff * 26)+30.9
                    if MPPTin <= 20:
                        l = 8
                        BB_x = 49
                        inverter_x = 9
                        inv_length = 1.5
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 15
                        BB_x = 69
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 29
                        BB_x = 40
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 29
                        BB_x = (diff * 13.5) + 40
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12

                    inverter.add_attdef('inverter_name', (-0.2, 0.4), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0.3), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, 0), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.3), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, -0.089), dxfattribs={'height': 0.09, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }

                    point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                             list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                    blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                        'xscale': l,
                        'yscale': 1.5,
                        'rotation': 0
                    })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp, doc,msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': Electricalscheme,
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (0.62, 0.98)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 0.2,
            'yscale': 0.3,
            'rotation': 0
        })
        blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name':GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (0.35, 0.9)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    point = (0.62, 0.98)
                    blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    point = (0.62, -0.07)
                    blockref = msp.add_blockref('GND', point, dxfattribs={
                        'xscale': 0.5,
                        'yscale': 0.5,
                        'rotation': 0
                    })
                    blockref.add_auto_attribs(ground_name)

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (0.62, 0.98)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (0.5, 0.98)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        point = (0.62, -0.07)
                        blockref = msp.add_blockref('GND', point, dxfattribs={
                            'xscale': 0.5,
                            'yscale': 0.5,
                            'rotation': 0
                        })
                        blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        point = (ns, 0.98) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 0.98) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        gnd_points = (point[0] + 0.65, 0.98)
                        blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        point = (ns, -0.07) + dots_list[-1]
                        gnd_points = (point[0] + 0.65, -0.065)
                        blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                            'xscale': 0.5,
                            'yscale': 0.5,
                            'rotation': 0
                        })
                        blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 0.15
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 0.15
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #
    pv_name = {
            'pvname': PVmodule+" "+ str(PvName)+" " + str(Wp),
            'XPOS': 1.3,
            'YPOS': 0.2
        }
    point = (ns, 0.6)+ dots_list[-1]
    block_s = msp.add_blockref('PV', point, dxfattribs={
            'xscale': 0.2,
            'yscale': 0.3,
            'rotation': 0
        })
    block_s.add_auto_attribs(pv_name)
    cable_name = {
            'cable_name': cabletype,
            'XPOS': 5,
            'YPOS': 0.2
        }
    combinerbox_name = {
        'combinerboxname': CombinerBox,
        'XPOS': 1.3,
        'YPOS': 0.2
    }
    point = (ns, 0.6) + dots_list[-1]
    point = (ns-0.05, 0.35) + dots_list[-1]
    block_ss = msp.add_blockref('COMBINERBOX', point, dxfattribs={
            'xscale': 0.2,
            'yscale': 0.3,
            'rotation': 0
        })
    block_ss.add_auto_attribs(combinerbox_name)
    inverter_name = {
            'inverter_names': InverterDCACtype+" "+paco+" "+ str(InvName),
            'XPOS': 1.3,
            'YPOS': 0.2
        }
    point = (ns-0.05, 0.22) + dots_list[-1]
    inverter_blocks = msp.add_blockref('INVERTER', point, dxfattribs={
            'xscale': 0.3,
            'yscale': 0.3,
            'rotation': 0
        })
    inverter_blocks.add_auto_attribs(inverter_name)
    point = (ns, 0.12) + dots_list[-1]
    block_ss = msp.add_blockref('PH', point, dxfattribs={
        'xscale': 5,
        'yscale': 0.3,
        'rotation': 0
    })
    block_ss.add_auto_attribs(cable_name)
    point = (inv_list[0].dxf.insert[0]+w_ls-0.29,inv_list[0].dxf.insert[1]-0.5)
        # x_pos = x_count
    grid_values = {
            'grid_name': gridconnection,
            'XPOS': 2,
            'YPOS': 0
        }
    blockref_grids=msp.add_blockref('GRIDCONNECTION', point, dxfattribs={
            'xscale': 0.1,
            'yscale': 0,
            'rotation': 0
        })
    blockref_grids.add_auto_attribs(grid_values)

    doc.saveas(dxf_file2_name)


def Ec_brazilian_dxf(res, pvdata, invdata, dxf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        tblock = doc.blocks.new(name='br')
        tblock2 = doc.blocks.new(name='trafo')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        bb_label = doc.blocks.new(name='BB_labels')
        inverter_label = doc.blocks.new(name='inverter_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        DASHED.add_line((4.8, 0.5), (4.9, 0.5))
        DASHED.add_line((5.3, 0.5), (5.4, 0.5))
        DASHED.add_line((5.8, 0.5), (5.9, 0.5))
        DASHED.add_line((6.3, 0.5), (6.4, 0.5))
        DASHED.add_line((6.8, 0.5), (6.9, 0.5))
        DASHED.add_line((7.3, 0.5), (7.4, 0.5))
        DASHED.add_line((7.8, 0.5), (7.9, 0.5))
        DASHED.add_line((8.4, 0.5), (8.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag3 = doc.blocks.new(name='FLAG3')
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_line((0, 1), (0.3, 0.6))
        flag3.add_line((0.5, 1), (0.3, 0.6))
        flag3.add_line((0.25, -0.2), (0.25, 0))
        flag3.add_line((0.25, -0.6), (0.25, -0.5))
        flag3.add_line((0.25, -1), (0.25, -0.9))
        flag3.add_line((0.25, -1.4), (0.25, -1.6))
        flag3.add_line((0.25, -2), (0.25, -2.1))
        flag3.add_line((0.25, -2.4), (0.25, -2.5))
        flag3.add_line((0.25, -2.9), (0.25, -3))
        flag3.add_line((0.25, -3.4), (0.25, -3.5))
        flag3.add_line((0.25, -3.9), (0.25, -4))
        flag3.add_line((0.25, -4.4), (0.25, -4.5))
        flag3.add_line((0.25, -4.9), (0.25, -5))
        flag3.add_line((0.25, 1), (0.25, 1.2))
        flag3.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag3.add_line((-0.5, 1.25), (0.2, 1.25))
        flag3.add_line((-0.5, 1.25), (-0.5, 1))
        flag3.add_line((-0.69, 1), (-0.29, 1))
        flag3.add_line((-0.59, 0.9), (-0.39, 0.9))
        flag3.add_line((-0.56, 0.8), (-0.42, 0.8))
        flag3.add_ellipse((-0.5, 1.12), major_axis=(0.5, 0), ratio=0.09)
        flag3.add_line((-1, 1.12), (-1.6, 1.12))
        flag3.add_line((-1.3, 1.12), (-1.3, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.1, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.5, 1.39))
        flag.add_attdef('NAME', (-2.5, 6.9), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME2', (-2.5, -6), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME3', (-1.2, 2), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME4', (0.35, -0.8), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME5', (0.35, -1.2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        DASHED.add_attdef('NAME3', (9, 0.39), dxfattribs={'height': 0.5, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 5), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 3})
        new_labels = doc.blocks.new(name='old_labels')

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 0.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls,maximum,flags
            MPPTin = key
            counts = count_parallelss
            print("maxi",max)
            maximum=max
            flags =False
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    color_list = [1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    # files = ['ModuleNew4.dxf']
                    # x = -1
                    # block_s = ['panels']
                    # # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
                    # for file in files:
                    #     x += 1
                    #     source_dxf = ezdxf.readfile(file)
                    #     # print(source_dxf.layout_names())
                    #     # for block in source_dxf.blocks:
                    #     #     print("blocksname", block.name)
                    #
                    #     if 'A$C20738' not in source_dxf.blocks:
                    #         print("Block  not defined.")
                    #     # print("len", len(source_dxf.blocks))
                    #     # print("blocx", block_s[x])
                    #     # for block in source_dxf.layout_names():
                    #     #     print("blocc", block)
                    #     importer = Importer(source_dxf, doc)
                    #     importer.import_block('module')
                    #     importer.finalize()
                    #     # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
                    #     # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
                    #     #     'height': 2000}).set_pos(point, align='CENTER')
                    #     block = doc.blocks.get('module')
                    #     # print("trarget block name and no of block module", block.name, len(doc.blocks))
                    #     # print("point", point)
                    #     # print("******", block.name)
                    #     block = block.block
                    #     # print(" initial base point beofe panels", block.dxf.base_point)
                    #     base_point = block.dxf.base_point
                    #     block.dxf.base_point = (1600, -48000)
                    #         # block.dxf.base_point = (-50000,-40000, 50000)
                    #     base_point = block.dxf.base_point
                    #     # print(" initial base point panels", block.dxf.base_point)
                    #     block_refernce = msp.add_blockref('module', block.dxf.base_point, dxfattribs={
                    #             'xscale': 10,
                    #             'yscale': 10,
                    #         })
                        # print("blockrefedpanels", block_refernce.dxf.insert)
                    for data in data_raw:
                        # colors += 1
                        # import pdb;
                        # pdb.set_trace()
                        if colors == len(color_list):
                            colors = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 52000
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("dotlist1***8",dots_list[-1])
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5":"Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                flags= True
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 49500
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                colors += 1
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count +=1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    for data in data_raw:
                        temp += 1
                        temp6 += 1

                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            x_count = 780 + point[0]
                            y_count =1 + point[1]
                            x_count += 800
                            y_count += 1000

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("********",block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 48000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            if frequency[data] == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=20000
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        inv_y = (diff * 26) + 75000
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                    if MPPTin <=20:
                        l = 20000
                        ls=26000
                        # BB_x=48
                        # inverter_x=9
                        inv_length=15000
                        invs_length=16500
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        # BB_x=60
                        # inverter_x = 18
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        ls=42000

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        BB_x=69
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        ls=82000
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x=(diff*13.5)+69
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    global blockref_inverter;
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    print("len of nedpoint",len(end_point))
                    # blockref_inverter.translate(50000.50,5000.50,100.50)
                    # blockref_inverter.scale(50000.50,50000.50,100.50)
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags,doc, msp)

            else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 20000
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        inv_y = (diff * 26)+75000
                    if MPPTin <= 20:
                        l = 20000
                        ls=26000
                        BB_x = 49
                        inverter_x = 9
                        inv_length = 15000
                        invs_length=16500
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        ls=42000
                        BB_x = 69
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        ls=82000
                        BB_x = 40
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x = (diff * 13.5) + 40
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags, doc,msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': Electricalscheme,
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (1000, 50000)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 200,
            'yscale': 300,
            'rotation': 0
        })
        blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name':GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (1000, 50000)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 500,
                        'yscale': 500,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    # point = (0.62, 0.98)
                    # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.3,
                    #     'rotation': 0
                    # })
                    # point = (0.62, -0.07)
                    # blockref = msp.add_blockref('GND', point, dxfattribs={
                    #     'xscale': 0.5,
                    #     'yscale': 0.5,
                    #     'rotation': 0
                    # })
                    # blockref.add_auto_attribs(ground_name)

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (6200, 2000)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (1000, 50000)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (0.62, -0.07)
                        # blockref = msp.add_blockref('GND', point, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        point = (ns, 2000) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 2000) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # gnd_points = (point[0] + 0.65, 0.98)
                        # blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (ns, -0.07) + dots_list[-1]
                        # gnd_points = (point[0] + 0.65, -0.065)
                        # blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 1500
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 1500
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #
    files = ['BR.dxf','TrafoUNIc.dxf']
    x = -1
    block_s=['br','trfo']
    # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
    for file in files:
        x+=1
        source_dxf = ezdxf.readfile(file)
        # print(source_dxf.layout_names())
        # for block in source_dxf.blocks:
        #     print("blocksname", block.name)

        if 'A$C20738' not in source_dxf.blocks:
            print("Block  not defined.")
        # print("len", len(source_dxf.blocks))
        # print("blocx",block_s[x])
        # for block in source_dxf.layout_names():
        #     print("blocc", block)
        importer = Importer(source_dxf, doc)
        if block_s[x] =='br' :
            importer.import_block('A$C12511')
        else:
            msp2=source_dxf.modelspace()
            def get_point():
                for text in msp2.query('TEXT'):
                    print("MTEXT content: {}".format(text.dxf.text))
                    if text.dxf.text == '??A':
                        print("hello")
                        print(text.dxf.insert)
                        point = text.dxf.insert
                        return point
            point = get_point()
            importer.import_block('A$C26352')
        importer.finalize()
        # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
        # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
        #     'height': 2000}).set_pos(point, align='CENTER')
        if block_s[x] == 'br':
            block = doc.blocks.get('A$C12511')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name,dots_list[-1])
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            print("*************first tuple",len(dots_list),dots_list[-1][0],type(dots_list[-1]) )
            if dots_list[-1][0] <=50000:
                dots_list[-1]+=((50000-dots_list[-1][0]),0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <=100000):
                dots_list[-1] += ((dots_list[-1][0]//2-30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0]//2-30000), 0)
                # while(dots_list<[-1][0]<= 80000):
                #     x_pos=dots_list[-1][0] // 2
                #
                # dots_list[-1] += ((x_pos - 30000), 0)
            print("*************secnd tuple", len(dots_list), dots_list[-1][0], type(dots_list[-1]))

            block.dxf.base_point = (dots_list[-1])*(-1) + (0,10000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C12511', block.dxf.base_point, dxfattribs={
            'xscale': 3,
            'yscale':2,
          })
            # print("blockrefed",block_refernce.dxf.insert)
        else:
            if maximum <= 20:
                x_l = 9780
                y_l = 1980
                x=61600
                y=18286
            elif maximum > 20 and maximum <= 60:
                x_l = 18620
                y_l = 1980
                x=62571
                y=5286
            elif maximum > 60 and maximum <= 150:
                x_l = 38650
                y_l = 1980
                x=62000
                y=-34700
            print("pointssssss",point)
            point_x2=(inv_list[0].dxf.insert[0]+x_l)
            point_y2=(inv_list[0].dxf.insert[1])-y_l
            ls = msp.add_line((-(x+point[0]), (point[1]+y)),(point_x2,point_y2))
            # msp.add_line((-84000, 23000), ls.dxf.end)
            # msp.add_circle(center=(2000, 20000), radius=20000, dxfattribs={'color': 5})
            # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
            #     'height': 2000}).set_pos(point, align='CENTER')
            block = doc.blocks.get('A$C26352')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name)
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            block.base_point = 0
            # ls=msp.add_line(inv_list[0].dxf.insert,(inv_list[0].dxf.insert[0],1000))
            print("maciumum",maximum,block.dxf.base_point)
            if dots_list[0][0] <= 50000:
                dots_list[0] -=((9000 - dots_list[0][0]), 0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[0] -= ((dots_list[0][0] // 2 - 30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0] // 2 - 30000), 0)

            if maximum <= 20 :
                l=4670
                l2=-9250
            elif maximum >20 and maximum <=60:
                l=79100
                l2=-10000
            elif maximum >60 and maximum <=150:
                l=278900
                l2=-12000
            print("dts*******",dots_list)
            print("lengthdd",dots_list[0][0],w_ls,inv_list[0].dxf.insert[0],inv_list[0].dxf.insert[1],(inv_list[0].dxf.insert[0] + w_ls))
            # point_x2 = inv_list[0].dxf.insert[0] -l
            # point_y2 = inv_list[0].dxf.insert[1] - l2
            # # print("88888",point_x2,point_y2)
            point_x2=(dots_list[0][0]) * (-1)
            point_y2=l2

            block.dxf.base_point = (point_x2,-point_y2)
            # block.dxf.base_point = (-50000, 50000, 50000)
            # block.dxf.base_point = (dots_list[-1]) * (-1) + (0, 10000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C26352', block.dxf.base_point, dxfattribs={
                'xscale': 20,
                'yscale': 20,
            })
            # msp.add_line(ls.dxf.start,block_refernce.dxf.insert)

        doc.saveas(dxf_file2_name)



def Ec_brazilian_trafobi_dxf(res, pvdata, invdata, dxf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        tblock = doc.blocks.new(name='br')
        tblock2 = doc.blocks.new(name='trafo')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        bb_label = doc.blocks.new(name='BB_labels')
        inverter_label = doc.blocks.new(name='inverter_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        DASHED.add_line((4.8, 0.5), (4.9, 0.5))
        DASHED.add_line((5.3, 0.5), (5.4, 0.5))
        DASHED.add_line((5.8, 0.5), (5.9, 0.5))
        DASHED.add_line((6.3, 0.5), (6.4, 0.5))
        DASHED.add_line((6.8, 0.5), (6.9, 0.5))
        DASHED.add_line((7.3, 0.5), (7.4, 0.5))
        DASHED.add_line((7.8, 0.5), (7.9, 0.5))
        DASHED.add_line((8.4, 0.5), (8.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag3 = doc.blocks.new(name='FLAG3')
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_line((0, 1), (0.3, 0.6))
        flag3.add_line((0.5, 1), (0.3, 0.6))
        flag3.add_line((0.25, -0.2), (0.25, 0))
        flag3.add_line((0.25, -0.6), (0.25, -0.5))
        flag3.add_line((0.25, -1), (0.25, -0.9))
        flag3.add_line((0.25, -1.4), (0.25, -1.6))
        flag3.add_line((0.25, -2), (0.25, -2.1))
        flag3.add_line((0.25, -2.4), (0.25, -2.5))
        flag3.add_line((0.25, -2.9), (0.25, -3))
        flag3.add_line((0.25, -3.4), (0.25, -3.5))
        flag3.add_line((0.25, -3.9), (0.25, -4))
        flag3.add_line((0.25, -4.4), (0.25, -4.5))
        flag3.add_line((0.25, -4.9), (0.25, -5))
        flag3.add_line((0.25, 1), (0.25, 1.2))
        flag3.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag3.add_line((-0.5, 1.25), (0.2, 1.25))
        flag3.add_line((-0.5, 1.25), (-0.5, 1))
        flag3.add_line((-0.69, 1), (-0.29, 1))
        flag3.add_line((-0.59, 0.9), (-0.39, 0.9))
        flag3.add_line((-0.56, 0.8), (-0.42, 0.8))
        flag3.add_ellipse((-0.5, 1.12), major_axis=(0.5, 0), ratio=0.09)
        flag3.add_line((-1, 1.12), (-1.6, 1.12))
        flag3.add_line((-1.3, 1.12), (-1.3, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.1, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.5, 1.39))
        flag.add_attdef('NAME', (-2.5, 6.9), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME2', (-2.5, -6), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME3', (-1.2, 2), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME4', (0.35, -0.8), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME5', (0.35, -1.2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        DASHED.add_attdef('NAME3', (9, 0.39), dxfattribs={'height': 0.5, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 5), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 3})
        new_labels = doc.blocks.new(name='old_labels')

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 0.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls,maximum,flags
            MPPTin = key
            counts = count_parallelss
            print("maxi",max)
            maximum=max
            flags =False
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    color_list = [1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    # files = ['ModuleNew4.dxf']
                    # x = -1
                    # block_s = ['panels']
                    # # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
                    # for file in files:
                    #     x += 1
                    #     source_dxf = ezdxf.readfile(file)
                    #     # print(source_dxf.layout_names())
                    #     # for block in source_dxf.blocks:
                    #     #     print("blocksname", block.name)
                    #
                    #     if 'A$C20738' not in source_dxf.blocks:
                    #         print("Block  not defined.")
                    #     # print("len", len(source_dxf.blocks))
                    #     # print("blocx", block_s[x])
                    #     # for block in source_dxf.layout_names():
                    #     #     print("blocc", block)
                    #     importer = Importer(source_dxf, doc)
                    #     importer.import_block('module')
                    #     importer.finalize()
                    #     # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
                    #     # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
                    #     #     'height': 2000}).set_pos(point, align='CENTER')
                    #     block = doc.blocks.get('module')
                    #     # print("trarget block name and no of block module", block.name, len(doc.blocks))
                    #     # print("point", point)
                    #     # print("******", block.name)
                    #     block = block.block
                    #     # print(" initial base point beofe panels", block.dxf.base_point)
                    #     base_point = block.dxf.base_point
                    #     block.dxf.base_point = (1600, -48000)
                    #         # block.dxf.base_point = (-50000,-40000, 50000)
                    #     base_point = block.dxf.base_point
                    #     # print(" initial base point panels", block.dxf.base_point)
                    #     block_refernce = msp.add_blockref('module', block.dxf.base_point, dxfattribs={
                    #             'xscale': 10,
                    #             'yscale': 10,
                    #         })
                        # print("blockrefedpanels", block_refernce.dxf.insert)
                    for data in data_raw:
                        # colors += 1
                        # import pdb;
                        # pdb.set_trace()
                        if colors == len(color_list):
                            colors = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 52000
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("dotlist1***8",dots_list[-1])
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5":"Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                flags= True
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 49500
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                colors += 1
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count +=1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    for data in data_raw:
                        temp += 1
                        temp6 += 1

                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            x_count = 780 + point[0]
                            y_count =1 + point[1]
                            x_count += 800
                            y_count += 1000

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("********",block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 48000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            if frequency[data] == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=20000
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        inv_y = (diff * 26) + 75000
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                    if MPPTin <=20:
                        l = 20000
                        ls=26000
                        # BB_x=48
                        # inverter_x=9
                        inv_length=15000
                        invs_length=16500
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        # BB_x=60
                        # inverter_x = 18
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        ls=42000

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        BB_x=69
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        ls=82000
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x=(diff*13.5)+69
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    global blockref_inverter;
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    print("len of nedpoint",len(end_point))
                    # blockref_inverter.translate(50000.50,5000.50,100.50)
                    # blockref_inverter.scale(50000.50,50000.50,100.50)
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags,doc, msp)

            else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 20000
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        inv_y = (diff * 26)+75000
                    if MPPTin <= 20:
                        l = 20000
                        ls=26000
                        BB_x = 49
                        inverter_x = 9
                        inv_length = 15000
                        invs_length=16500
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        ls=42000
                        BB_x = 69
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        ls=82000
                        BB_x = 40
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x = (diff * 13.5) + 40
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags, doc,msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': Electricalscheme,
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (1000, 50000)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 200,
            'yscale': 300,
            'rotation': 0
        })
        blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name':GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (1000, 50000)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 500,
                        'yscale': 500,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    # point = (0.62, 0.98)
                    # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.3,
                    #     'rotation': 0
                    # })
                    # point = (0.62, -0.07)
                    # blockref = msp.add_blockref('GND', point, dxfattribs={
                    #     'xscale': 0.5,
                    #     'yscale': 0.5,
                    #     'rotation': 0
                    # })
                    # blockref.add_auto_attribs(ground_name)

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (6200, 2000)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (1000, 50000)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (0.62, -0.07)
                        # blockref = msp.add_blockref('GND', point, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        point = (ns, 2000) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 2000) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # gnd_points = (point[0] + 0.65, 0.98)
                        # blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (ns, -0.07) + dots_list[-1]
                        # gnd_points = (point[0] + 0.65, -0.065)
                        # blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 1500
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 1500
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #
    files = ['BR.dxf','TrafoBIc.dxf']
    x = -1
    block_s=['br','trfo']
    # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
    for file in files:
        x+=1
        source_dxf = ezdxf.readfile(file)
        # print(source_dxf.layout_names())
        # for block in source_dxf.blocks:
        #     print("blocksname", block.name)

        if 'A$C20738' not in source_dxf.blocks:
            print("Block  not defined.")
        # print("len", len(source_dxf.blocks))
        # print("blocx",block_s[x])
        # for block in source_dxf.layout_names():
        #     print("blocc", block)
        importer = Importer(source_dxf, doc)
        if block_s[x] =='br' :
            importer.import_block('A$C12511')
        else:
            msp2=source_dxf.modelspace()
            def get_point():
                for text in msp2.query('TEXT'):
                    print("MTEXT content: {}".format(text.dxf.text))
                    if text.dxf.text == '??A':
                        print("hello")
                        print(text.dxf.insert)
                        point = text.dxf.insert
                        return point
            point = get_point()
            importer.import_block('A$C26352')
        importer.finalize()
        # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
        # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
        #     'height': 2000}).set_pos(point, align='CENTER')
        if block_s[x] == 'br':
            block = doc.blocks.get('A$C12511')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name,dots_list[-1])
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            print("*************first tuple",len(dots_list),dots_list[-1][0],type(dots_list[-1]) )
            if dots_list[-1][0] <=50000:
                dots_list[-1]+=((50000-dots_list[-1][0]),0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <=100000):
                dots_list[-1] += ((dots_list[-1][0]//2-30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0]//2-30000), 0)
                # while(dots_list<[-1][0]<= 80000):
                #     x_pos=dots_list[-1][0] // 2
                #
                # dots_list[-1] += ((x_pos - 30000), 0)
            print("*************secnd tuple", len(dots_list), dots_list[-1][0], type(dots_list[-1]))

            block.dxf.base_point = (dots_list[-1])*(-1) + (0,10000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C12511', block.dxf.base_point, dxfattribs={
            'xscale': 3,
            'yscale':2,
          })
            # print("blockrefed",block_refernce.dxf.insert)
        else:
            if maximum <= 20:
                x_l = 9780
                y_l = 1980
                x=61600
                y=18286
            elif maximum > 20 and maximum <= 60:
                x_l = 18620
                y_l = 1980
                x=62571
                y=5286
            elif maximum > 60 and maximum <= 150:
                x_l = 38650
                y_l = 1980
                x=62000
                y=-34700
            print("pointssssss",point)
            point_x2=(inv_list[0].dxf.insert[0]+x_l)
            point_y2=(inv_list[0].dxf.insert[1])-y_l
            ls = msp.add_line((-(x+point[0]), (point[1]+y)),(point_x2,point_y2))
            # msp.add_line((-84000, 23000), ls.dxf.end)
            # msp.add_circle(center=(2000, 20000), radius=20000, dxfattribs={'color': 5})
            # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
            #     'height': 2000}).set_pos(point, align='CENTER')
            block = doc.blocks.get('A$C26352')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name)
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            block.base_point = 0
            # ls=msp.add_line(inv_list[0].dxf.insert,(inv_list[0].dxf.insert[0],1000))
            print("maciumum",maximum,block.dxf.base_point)
            if dots_list[0][0] <= 50000:
                dots_list[0] -=((9000 - dots_list[0][0]), 0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[0] -= ((dots_list[0][0] // 2 - 30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0] // 2 - 30000), 0)

            if maximum <= 20 :
                l=4670
                l2=-9250
            elif maximum >20 and maximum <=60:
                l=79100
                l2=-10000
            elif maximum >60 and maximum <=150:
                l=278900
                l2=-12000
            print("dts*******",dots_list)
            print("lengthdd",dots_list[0][0],w_ls,inv_list[0].dxf.insert[0],inv_list[0].dxf.insert[1],(inv_list[0].dxf.insert[0] + w_ls))
            # point_x2 = inv_list[0].dxf.insert[0] -l
            # point_y2 = inv_list[0].dxf.insert[1] - l2
            # # print("88888",point_x2,point_y2)
            point_x2=(dots_list[0][0]) * (-1)
            point_y2=l2

            block.dxf.base_point = (point_x2,-point_y2)
            # block.dxf.base_point = (-50000, 50000, 50000)
            # block.dxf.base_point = (dots_list[-1]) * (-1) + (0, 10000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C26352', block.dxf.base_point, dxfattribs={
                'xscale': 20,
                'yscale': 20,
            })
            # msp.add_line(ls.dxf.start,block_refernce.dxf.insert)

        doc.saveas(dxf_file2_name)



def Ec_brazilian_trafotri_dxf(res, pvdata, invdata, dxf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        tblock = doc.blocks.new(name='br')
        tblock2 = doc.blocks.new(name='trafo')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        bb_label = doc.blocks.new(name='BB_labels')
        inverter_label = doc.blocks.new(name='inverter_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        DASHED.add_line((4.8, 0.5), (4.9, 0.5))
        DASHED.add_line((5.3, 0.5), (5.4, 0.5))
        DASHED.add_line((5.8, 0.5), (5.9, 0.5))
        DASHED.add_line((6.3, 0.5), (6.4, 0.5))
        DASHED.add_line((6.8, 0.5), (6.9, 0.5))
        DASHED.add_line((7.3, 0.5), (7.4, 0.5))
        DASHED.add_line((7.8, 0.5), (7.9, 0.5))
        DASHED.add_line((8.4, 0.5), (8.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag3 = doc.blocks.new(name='FLAG3')
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_line((0, 1), (0.3, 0.6))
        flag3.add_line((0.5, 1), (0.3, 0.6))
        flag3.add_line((0.25, -0.2), (0.25, 0))
        flag3.add_line((0.25, -0.6), (0.25, -0.5))
        flag3.add_line((0.25, -1), (0.25, -0.9))
        flag3.add_line((0.25, -1.4), (0.25, -1.6))
        flag3.add_line((0.25, -2), (0.25, -2.1))
        flag3.add_line((0.25, -2.4), (0.25, -2.5))
        flag3.add_line((0.25, -2.9), (0.25, -3))
        flag3.add_line((0.25, -3.4), (0.25, -3.5))
        flag3.add_line((0.25, -3.9), (0.25, -4))
        flag3.add_line((0.25, -4.4), (0.25, -4.5))
        flag3.add_line((0.25, -4.9), (0.25, -5))
        flag3.add_line((0.25, 1), (0.25, 1.2))
        flag3.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag3.add_line((-0.5, 1.25), (0.2, 1.25))
        flag3.add_line((-0.5, 1.25), (-0.5, 1))
        flag3.add_line((-0.69, 1), (-0.29, 1))
        flag3.add_line((-0.59, 0.9), (-0.39, 0.9))
        flag3.add_line((-0.56, 0.8), (-0.42, 0.8))
        flag3.add_ellipse((-0.5, 1.12), major_axis=(0.5, 0), ratio=0.09)
        flag3.add_line((-1, 1.12), (-1.6, 1.12))
        flag3.add_line((-1.3, 1.12), (-1.3, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.1, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.5, 1.39))
        flag.add_attdef('NAME', (-2.5, 6.9), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME2', (-2.5, -6), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME3', (-1.2, 2), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME4', (0.35, -0.8), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME5', (0.35, -1.2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        DASHED.add_attdef('NAME3', (9, 0.39), dxfattribs={'height': 0.5, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 5), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 3})
        new_labels = doc.blocks.new(name='old_labels')

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 0.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls,maximum,flags
            MPPTin = key
            counts = count_parallelss
            print("maxi",max)
            maximum=max
            flags =False
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    color_list = [1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    # files = ['ModuleNew4.dxf']
                    # x = -1
                    # block_s = ['panels']
                    # # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
                    # for file in files:
                    #     x += 1
                    #     source_dxf = ezdxf.readfile(file)
                    #     # print(source_dxf.layout_names())
                    #     # for block in source_dxf.blocks:
                    #     #     print("blocksname", block.name)
                    #
                    #     if 'A$C20738' not in source_dxf.blocks:
                    #         print("Block  not defined.")
                    #     # print("len", len(source_dxf.blocks))
                    #     # print("blocx", block_s[x])
                    #     # for block in source_dxf.layout_names():
                    #     #     print("blocc", block)
                    #     importer = Importer(source_dxf, doc)
                    #     importer.import_block('module')
                    #     importer.finalize()
                    #     # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
                    #     # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
                    #     #     'height': 2000}).set_pos(point, align='CENTER')
                    #     block = doc.blocks.get('module')
                    #     # print("trarget block name and no of block module", block.name, len(doc.blocks))
                    #     # print("point", point)
                    #     # print("******", block.name)
                    #     block = block.block
                    #     # print(" initial base point beofe panels", block.dxf.base_point)
                    #     base_point = block.dxf.base_point
                    #     block.dxf.base_point = (1600, -48000)
                    #         # block.dxf.base_point = (-50000,-40000, 50000)
                    #     base_point = block.dxf.base_point
                    #     # print(" initial base point panels", block.dxf.base_point)
                    #     block_refernce = msp.add_blockref('module', block.dxf.base_point, dxfattribs={
                    #             'xscale': 10,
                    #             'yscale': 10,
                    #         })
                        # print("blockrefedpanels", block_refernce.dxf.insert)
                    for data in data_raw:
                        # colors += 1
                        # import pdb;
                        # pdb.set_trace()
                        if colors == len(color_list):
                            colors = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 52000
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("dotlist1***8",dots_list[-1])
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5":"Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                flags= True
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 49500
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                colors += 1
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count +=1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    for data in data_raw:
                        temp += 1
                        temp6 += 1

                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            x_count = 780 + point[0]
                            y_count =1 + point[1]
                            x_count += 800
                            y_count += 1000

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("********",block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 48000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            if frequency[data] == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=20000
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        inv_y = (diff * 26) + 75000
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                    if MPPTin <=20:
                        l = 20000
                        ls=26000
                        # BB_x=48
                        # inverter_x=9
                        inv_length=15000
                        invs_length=16500
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        # BB_x=60
                        # inverter_x = 18
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        ls=42000

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        BB_x=69
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        ls=82000
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x=(diff*13.5)+69
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    global blockref_inverter;
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    print("len of nedpoint",len(end_point))
                    # blockref_inverter.translate(50000.50,5000.50,100.50)
                    # blockref_inverter.scale(50000.50,50000.50,100.50)
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags,doc, msp)

            else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 20000
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        inv_y = (diff * 26)+75000
                    if MPPTin <= 20:
                        l = 20000
                        ls=26000
                        BB_x = 49
                        inverter_x = 9
                        inv_length = 15000
                        invs_length=16500
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        ls=42000
                        BB_x = 69
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        ls=82000
                        BB_x = 40
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x = (diff * 13.5) + 40
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags, doc,msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': Electricalscheme,
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (1000, 50000)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 200,
            'yscale': 300,
            'rotation': 0
        })
        blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name':GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (1000, 50000)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 500,
                        'yscale': 500,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    # point = (0.62, 0.98)
                    # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.3,
                    #     'rotation': 0
                    # })
                    # point = (0.62, -0.07)
                    # blockref = msp.add_blockref('GND', point, dxfattribs={
                    #     'xscale': 0.5,
                    #     'yscale': 0.5,
                    #     'rotation': 0
                    # })
                    # blockref.add_auto_attribs(ground_name)

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (6200, 2000)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (1000, 50000)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (0.62, -0.07)
                        # blockref = msp.add_blockref('GND', point, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        point = (ns, 2000) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 2000) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # gnd_points = (point[0] + 0.65, 0.98)
                        # blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (ns, -0.07) + dots_list[-1]
                        # gnd_points = (point[0] + 0.65, -0.065)
                        # blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 1500
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 1500
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #
    files = ['BR.dxf','TrafoTRIc.dxf']
    x = -1
    block_s=['br','trfo']
    # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
    for file in files:
        x+=1
        source_dxf = ezdxf.readfile(file)
        # print(source_dxf.layout_names())
        # for block in source_dxf.blocks:
        #     print("blocksname", block.name)

        if 'A$C20738' not in source_dxf.blocks:
            print("Block  not defined.")
        # print("len", len(source_dxf.blocks))
        # print("blocx",block_s[x])
        # for block in source_dxf.layout_names():
        #     print("blocc", block)
        importer = Importer(source_dxf, doc)
        if block_s[x] =='br' :
            importer.import_block('A$C12511')
        else:
            msp2=source_dxf.modelspace()
            def get_point():
                for text in msp2.query('TEXT'):
                    print("MTEXT content: {}".format(text.dxf.text))
                    if text.dxf.text == '??A':
                        print("hello")
                        print(text.dxf.insert)
                        point = text.dxf.insert
                        return point
            point = get_point()
            importer.import_block('A$C26352')
        importer.finalize()
        # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
        # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
        #     'height': 2000}).set_pos(point, align='CENTER')
        if block_s[x] == 'br':
            block = doc.blocks.get('A$C12511')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name,dots_list[-1])
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            print("*************first tuple",len(dots_list),dots_list[-1][0],type(dots_list[-1]) )
            if dots_list[-1][0] <=50000:
                dots_list[-1]+=((50000-dots_list[-1][0]),0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <=100000):
                dots_list[-1] += ((dots_list[-1][0]//2-30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0]//2-30000), 0)
                # while(dots_list<[-1][0]<= 80000):
                #     x_pos=dots_list[-1][0] // 2
                #
                # dots_list[-1] += ((x_pos - 30000), 0)
            print("*************secnd tuple", len(dots_list), dots_list[-1][0], type(dots_list[-1]))

            block.dxf.base_point = (dots_list[-1])*(-1) + (0,10000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C12511', block.dxf.base_point, dxfattribs={
            'xscale': 3,
            'yscale':2,
          })
            # print("blockrefed",block_refernce.dxf.insert)
        else:
            if maximum <= 20:
                x_l = 9780
                y_l = 1980
                x=61600
                y=18286
            elif maximum > 20 and maximum <= 60:
                x_l = 18620
                y_l = 1980
                x=62571
                y=5286
            elif maximum > 60 and maximum <= 150:
                x_l = 38650
                y_l = 1980
                x=62000
                y=-34700
            print("pointssssss",point)
            point_x2=(inv_list[0].dxf.insert[0]+x_l)
            point_y2=(inv_list[0].dxf.insert[1])-y_l
            ls = msp.add_line((-(x+point[0]), (point[1]+y)),(point_x2,point_y2))
            # msp.add_line((-84000, 23000), ls.dxf.end)
            # msp.add_circle(center=(2000, 20000), radius=20000, dxfattribs={'color': 5})
            # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
            #     'height': 2000}).set_pos(point, align='CENTER')
            block = doc.blocks.get('A$C26352')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name)
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            block.base_point = 0
            # ls=msp.add_line(inv_list[0].dxf.insert,(inv_list[0].dxf.insert[0],1000))
            print("maciumum",maximum,block.dxf.base_point)
            if dots_list[0][0] <= 50000:
                dots_list[0] -=((9000 - dots_list[0][0]), 0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[0] -= ((dots_list[0][0] // 2 - 30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0] // 2 - 30000), 0)

            if maximum <= 20 :
                l=4670
                l2=-9250
            elif maximum >20 and maximum <=60:
                l=79100
                l2=-10000
            elif maximum >60 and maximum <=150:
                l=278900
                l2=-12000
            print("dts*******",dots_list)
            print("lengthdd",dots_list[0][0],w_ls,inv_list[0].dxf.insert[0],inv_list[0].dxf.insert[1],(inv_list[0].dxf.insert[0] + w_ls))
            # point_x2 = inv_list[0].dxf.insert[0] -l
            # point_y2 = inv_list[0].dxf.insert[1] - l2
            # # print("88888",point_x2,point_y2)
            point_x2=(dots_list[0][0]) * (-1)
            point_y2=l2

            block.dxf.base_point = (point_x2,-point_y2)
            # block.dxf.base_point = (-50000, 50000, 50000)
            # block.dxf.base_point = (dots_list[-1]) * (-1) + (0, 10000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C26352', block.dxf.base_point, dxfattribs={
                'xscale': 20,
                'yscale': 20,
            })
            # msp.add_line(ls.dxf.start,block_refernce.dxf.insert)

        doc.saveas(dxf_file2_name)



def Ec_brazilian_notrafouni_dxf(res, pvdata, invdata, dxf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        tblock = doc.blocks.new(name='br')
        tblock2 = doc.blocks.new(name='trafo')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        bb_label = doc.blocks.new(name='BB_labels')
        inverter_label = doc.blocks.new(name='inverter_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        DASHED.add_line((4.8, 0.5), (4.9, 0.5))
        DASHED.add_line((5.3, 0.5), (5.4, 0.5))
        DASHED.add_line((5.8, 0.5), (5.9, 0.5))
        DASHED.add_line((6.3, 0.5), (6.4, 0.5))
        DASHED.add_line((6.8, 0.5), (6.9, 0.5))
        DASHED.add_line((7.3, 0.5), (7.4, 0.5))
        DASHED.add_line((7.8, 0.5), (7.9, 0.5))
        DASHED.add_line((8.4, 0.5), (8.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag3 = doc.blocks.new(name='FLAG3')
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_line((0, 1), (0.3, 0.6))
        flag3.add_line((0.5, 1), (0.3, 0.6))
        flag3.add_line((0.25, -0.2), (0.25, 0))
        flag3.add_line((0.25, -0.6), (0.25, -0.5))
        flag3.add_line((0.25, -1), (0.25, -0.9))
        flag3.add_line((0.25, -1.4), (0.25, -1.6))
        flag3.add_line((0.25, -2), (0.25, -2.1))
        flag3.add_line((0.25, -2.4), (0.25, -2.5))
        flag3.add_line((0.25, -2.9), (0.25, -3))
        flag3.add_line((0.25, -3.4), (0.25, -3.5))
        flag3.add_line((0.25, -3.9), (0.25, -4))
        flag3.add_line((0.25, -4.4), (0.25, -4.5))
        flag3.add_line((0.25, -4.9), (0.25, -5))
        flag3.add_line((0.25, 1), (0.25, 1.2))
        flag3.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag3.add_line((-0.5, 1.25), (0.2, 1.25))
        flag3.add_line((-0.5, 1.25), (-0.5, 1))
        flag3.add_line((-0.69, 1), (-0.29, 1))
        flag3.add_line((-0.59, 0.9), (-0.39, 0.9))
        flag3.add_line((-0.56, 0.8), (-0.42, 0.8))
        flag3.add_ellipse((-0.5, 1.12), major_axis=(0.5, 0), ratio=0.09)
        flag3.add_line((-1, 1.12), (-1.6, 1.12))
        flag3.add_line((-1.3, 1.12), (-1.3, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.1, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.5, 1.39))
        flag.add_attdef('NAME', (-2.5, 6.9), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME2', (-2.5, -6), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME3', (-1.2, 2), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME4', (0.35, -0.8), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME5', (0.35, -1.2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        DASHED.add_attdef('NAME3', (9, 0.39), dxfattribs={'height': 0.5, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 5), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 3})
        new_labels = doc.blocks.new(name='old_labels')

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 0.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls,maximum,flags
            MPPTin = key
            counts = count_parallelss
            print("maxi",max)
            maximum=max
            flags =False
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    color_list = [1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    # files = ['ModuleNew4.dxf']
                    # x = -1
                    # block_s = ['panels']
                    # # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
                    # for file in files:
                    #     x += 1
                    #     source_dxf = ezdxf.readfile(file)
                    #     # print(source_dxf.layout_names())
                    #     # for block in source_dxf.blocks:
                    #     #     print("blocksname", block.name)
                    #
                    #     if 'A$C20738' not in source_dxf.blocks:
                    #         print("Block  not defined.")
                    #     # print("len", len(source_dxf.blocks))
                    #     # print("blocx", block_s[x])
                    #     # for block in source_dxf.layout_names():
                    #     #     print("blocc", block)
                    #     importer = Importer(source_dxf, doc)
                    #     importer.import_block('module')
                    #     importer.finalize()
                    #     # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
                    #     # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
                    #     #     'height': 2000}).set_pos(point, align='CENTER')
                    #     block = doc.blocks.get('module')
                    #     # print("trarget block name and no of block module", block.name, len(doc.blocks))
                    #     # print("point", point)
                    #     # print("******", block.name)
                    #     block = block.block
                    #     # print(" initial base point beofe panels", block.dxf.base_point)
                    #     base_point = block.dxf.base_point
                    #     block.dxf.base_point = (1600, -48000)
                    #         # block.dxf.base_point = (-50000,-40000, 50000)
                    #     base_point = block.dxf.base_point
                    #     # print(" initial base point panels", block.dxf.base_point)
                    #     block_refernce = msp.add_blockref('module', block.dxf.base_point, dxfattribs={
                    #             'xscale': 10,
                    #             'yscale': 10,
                    #         })
                        # print("blockrefedpanels", block_refernce.dxf.insert)
                    for data in data_raw:
                        # colors += 1
                        # import pdb;
                        # pdb.set_trace()
                        if colors == len(color_list):
                            colors = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 52000
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("dotlist1***8",dots_list[-1])
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5":"Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                flags= True
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 49500
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                colors += 1
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count +=1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    for data in data_raw:
                        temp += 1
                        temp6 += 1

                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            x_count = 780 + point[0]
                            y_count =1 + point[1]
                            x_count += 800
                            y_count += 1000

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("********",block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 48000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            if frequency[data] == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=20000
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        inv_y = (diff * 26) + 75000
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                    if MPPTin <=20:
                        l = 20000
                        ls=26000
                        # BB_x=48
                        # inverter_x=9
                        inv_length=15000
                        invs_length=16500
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        # BB_x=60
                        # inverter_x = 18
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        ls=42000

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        BB_x=69
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        ls=82000
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x=(diff*13.5)+69
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    global blockref_inverter;
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    print("len of nedpoint",len(end_point))
                    # blockref_inverter.translate(50000.50,5000.50,100.50)
                    # blockref_inverter.scale(50000.50,50000.50,100.50)
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags,doc, msp)

            else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 20000
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        inv_y = (diff * 26)+75000
                    if MPPTin <= 20:
                        l = 20000
                        ls=26000
                        BB_x = 49
                        inverter_x = 9
                        inv_length = 15000
                        invs_length=16500
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        ls=42000
                        BB_x = 69
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        ls=82000
                        BB_x = 40
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x = (diff * 13.5) + 40
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags, doc,msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': Electricalscheme,
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (1000, 50000)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 200,
            'yscale': 300,
            'rotation': 0
        })
        blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name':GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (1000, 50000)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 500,
                        'yscale': 500,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    # point = (0.62, 0.98)
                    # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.3,
                    #     'rotation': 0
                    # })
                    # point = (0.62, -0.07)
                    # blockref = msp.add_blockref('GND', point, dxfattribs={
                    #     'xscale': 0.5,
                    #     'yscale': 0.5,
                    #     'rotation': 0
                    # })
                    # blockref.add_auto_attribs(ground_name)

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (6200, 2000)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (1000, 50000)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (0.62, -0.07)
                        # blockref = msp.add_blockref('GND', point, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        point = (ns, 2000) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 2000) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # gnd_points = (point[0] + 0.65, 0.98)
                        # blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (ns, -0.07) + dots_list[-1]
                        # gnd_points = (point[0] + 0.65, -0.065)
                        # blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 1500
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 1500
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #
    files = ['BR.dxf','NOTrafoUNIc.dxf']
    x = -1
    block_s=['br','trfo']
    # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
    for file in files:
        x+=1
        source_dxf = ezdxf.readfile(file)
        # print(source_dxf.layout_names())
        # for block in source_dxf.blocks:
        #     print("blocksname", block.name)

        if 'A$C20738' not in source_dxf.blocks:
            print("Block  not defined.")
        # print("len", len(source_dxf.blocks))
        # print("blocx",block_s[x])
        # for block in source_dxf.layout_names():
        #     print("blocc", block)
        importer = Importer(source_dxf, doc)
        if block_s[x] =='br' :
            importer.import_block('A$C12511')
        else:
            msp2=source_dxf.modelspace()
            def get_point():
                for text in msp2.query('TEXT'):
                    print("MTEXT content: {}".format(text.dxf.text))
                    if text.dxf.text == '??A':
                        print("hello")
                        print(text.dxf.insert)
                        point = text.dxf.insert
                        return point
            point = get_point()
            importer.import_block('A$C29800')
        importer.finalize()
        # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
        # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
        #     'height': 2000}).set_pos(point, align='CENTER')
        if block_s[x] == 'br':
            block = doc.blocks.get('A$C12511')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name,dots_list[-1])
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            print("*************first tuple",len(dots_list),dots_list[-1][0],type(dots_list[-1]) )
            if dots_list[-1][0] <=50000:
                dots_list[-1]+=((50000-dots_list[-1][0]),0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <=100000):
                dots_list[-1] += ((dots_list[-1][0]//2-30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0]//2-30000), 0)
                # while(dots_list<[-1][0]<= 80000):
                #     x_pos=dots_list[-1][0] // 2
                #
                # dots_list[-1] += ((x_pos - 30000), 0)
            print("*************secnd tuple", len(dots_list), dots_list[-1][0], type(dots_list[-1]))

            block.dxf.base_point = (dots_list[-1])*(-1) + (0,10000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C12511', block.dxf.base_point, dxfattribs={
            'xscale': 3,
            'yscale':2,
          })
            # print("blockrefed",block_refernce.dxf.insert)
        else:
            if maximum <= 20:
                x_l = 13680
                y_l = 1980
                x=61600
                y=16286
            elif maximum > 20 and maximum <= 60:
                x_l = 18520
                y_l = 1980
                x=62571
                y=1286
            elif maximum > 60 and maximum <= 150:
                x_l = 38650
                y_l = 1980
                x=62000
                y=-40000
            print("pointssssss",point,y)
            point_x2=(inv_list[0].dxf.insert[0]+x_l)
            point_y2=(inv_list[0].dxf.insert[1])-y_l
            ls = msp.add_line((-(x+point[0]), (point[1]+y)),(point_x2,point_y2))
            # msp.add_line((-84000, 23000), ls.dxf.end)
            # msp.add_circle(center=(2000, 20000), radius=20000, dxfattribs={'color': 5})
            # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
            #     'height': 2000}).set_pos(point, align='CENTER')
            block = doc.blocks.get('A$C29800')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name)
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            block.base_point = 0
            # ls=msp.add_line(inv_list[0].dxf.insert,(inv_list[0].dxf.insert[0],1000))
            print("maciumum",maximum,block.dxf.base_point)
            if dots_list[0][0] <= 50000:
                dots_list[0] -=((14700 - dots_list[0][0]), 0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[0] -= ((dots_list[0][0] // 2 - 30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0] // 2 - 30000), 0)

            if maximum <= 20 :
                l=4670
                l2=-10200
            elif maximum >20 and maximum <=60:
                l=79100
                l2=-11000
            elif maximum >60 and maximum <=150:
                l=278900
                l2=-13200
            print("dts*******",dots_list)
            print("lengthdd",dots_list[0][0],w_ls,inv_list[0].dxf.insert[0],inv_list[0].dxf.insert[1],(inv_list[0].dxf.insert[0] + w_ls))
            # point_x2 = inv_list[0].dxf.insert[0] -l
            # point_y2 = inv_list[0].dxf.insert[1] - l2
            # # print("88888",point_x2,point_y2)
            point_x2=(dots_list[0][0]) * (-1)
            point_y2=l2

            block.dxf.base_point = (point_x2,-point_y2)
            # block.dxf.base_point = (-50000, 50000, 50000)
            # block.dxf.base_point = (dots_list[-1]) * (-1) + (0, 10000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C29800', block.dxf.base_point, dxfattribs={
                'xscale': 20,
                'yscale': 20,
            })
            # msp.add_line(ls.dxf.start,block_refernce.dxf.insert)

        doc.saveas(dxf_file2_name)


def Ec_brazilian_notrafobi_dxf(res, pvdata, invdata, dxf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        tblock = doc.blocks.new(name='br')
        tblock2 = doc.blocks.new(name='trafo')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        bb_label = doc.blocks.new(name='BB_labels')
        inverter_label = doc.blocks.new(name='inverter_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        DASHED.add_line((4.8, 0.5), (4.9, 0.5))
        DASHED.add_line((5.3, 0.5), (5.4, 0.5))
        DASHED.add_line((5.8, 0.5), (5.9, 0.5))
        DASHED.add_line((6.3, 0.5), (6.4, 0.5))
        DASHED.add_line((6.8, 0.5), (6.9, 0.5))
        DASHED.add_line((7.3, 0.5), (7.4, 0.5))
        DASHED.add_line((7.8, 0.5), (7.9, 0.5))
        DASHED.add_line((8.4, 0.5), (8.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag3 = doc.blocks.new(name='FLAG3')
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_line((0, 1), (0.3, 0.6))
        flag3.add_line((0.5, 1), (0.3, 0.6))
        flag3.add_line((0.25, -0.2), (0.25, 0))
        flag3.add_line((0.25, -0.6), (0.25, -0.5))
        flag3.add_line((0.25, -1), (0.25, -0.9))
        flag3.add_line((0.25, -1.4), (0.25, -1.6))
        flag3.add_line((0.25, -2), (0.25, -2.1))
        flag3.add_line((0.25, -2.4), (0.25, -2.5))
        flag3.add_line((0.25, -2.9), (0.25, -3))
        flag3.add_line((0.25, -3.4), (0.25, -3.5))
        flag3.add_line((0.25, -3.9), (0.25, -4))
        flag3.add_line((0.25, -4.4), (0.25, -4.5))
        flag3.add_line((0.25, -4.9), (0.25, -5))
        flag3.add_line((0.25, 1), (0.25, 1.2))
        flag3.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag3.add_line((-0.5, 1.25), (0.2, 1.25))
        flag3.add_line((-0.5, 1.25), (-0.5, 1))
        flag3.add_line((-0.69, 1), (-0.29, 1))
        flag3.add_line((-0.59, 0.9), (-0.39, 0.9))
        flag3.add_line((-0.56, 0.8), (-0.42, 0.8))
        flag3.add_ellipse((-0.5, 1.12), major_axis=(0.5, 0), ratio=0.09)
        flag3.add_line((-1, 1.12), (-1.6, 1.12))
        flag3.add_line((-1.3, 1.12), (-1.3, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.1, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.5, 1.39))
        flag.add_attdef('NAME', (-2.5, 6.9), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME2', (-2.5, -6), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME3', (-1.2, 2), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME4', (0.35, -0.8), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME5', (0.35, -1.2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        DASHED.add_attdef('NAME3', (9, 0.39), dxfattribs={'height': 0.5, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 5), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 3})
        new_labels = doc.blocks.new(name='old_labels')

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 0.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls,maximum,flags
            MPPTin = key
            counts = count_parallelss
            print("maxi",max)
            maximum=max
            flags =False
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    color_list = [1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    # files = ['ModuleNew4.dxf']
                    # x = -1
                    # block_s = ['panels']
                    # # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
                    # for file in files:
                    #     x += 1
                    #     source_dxf = ezdxf.readfile(file)
                    #     # print(source_dxf.layout_names())
                    #     # for block in source_dxf.blocks:
                    #     #     print("blocksname", block.name)
                    #
                    #     if 'A$C20738' not in source_dxf.blocks:
                    #         print("Block  not defined.")
                    #     # print("len", len(source_dxf.blocks))
                    #     # print("blocx", block_s[x])
                    #     # for block in source_dxf.layout_names():
                    #     #     print("blocc", block)
                    #     importer = Importer(source_dxf, doc)
                    #     importer.import_block('module')
                    #     importer.finalize()
                    #     # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
                    #     # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
                    #     #     'height': 2000}).set_pos(point, align='CENTER')
                    #     block = doc.blocks.get('module')
                    #     # print("trarget block name and no of block module", block.name, len(doc.blocks))
                    #     # print("point", point)
                    #     # print("******", block.name)
                    #     block = block.block
                    #     # print(" initial base point beofe panels", block.dxf.base_point)
                    #     base_point = block.dxf.base_point
                    #     block.dxf.base_point = (1600, -48000)
                    #         # block.dxf.base_point = (-50000,-40000, 50000)
                    #     base_point = block.dxf.base_point
                    #     # print(" initial base point panels", block.dxf.base_point)
                    #     block_refernce = msp.add_blockref('module', block.dxf.base_point, dxfattribs={
                    #             'xscale': 10,
                    #             'yscale': 10,
                    #         })
                        # print("blockrefedpanels", block_refernce.dxf.insert)
                    for data in data_raw:
                        # colors += 1
                        # import pdb;
                        # pdb.set_trace()
                        if colors == len(color_list):
                            colors = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 52000
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("dotlist1***8",dots_list[-1])
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5":"Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                flags= True
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 49500
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                colors += 1
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count +=1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    for data in data_raw:
                        temp += 1
                        temp6 += 1

                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            x_count = 780 + point[0]
                            y_count =1 + point[1]
                            x_count += 800
                            y_count += 1000

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("********",block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 48000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            if frequency[data] == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=20000
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        inv_y = (diff * 26) + 75000
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                    if MPPTin <=20:
                        l = 20000
                        ls=26000
                        # BB_x=48
                        # inverter_x=9
                        inv_length=15000
                        invs_length=16500
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        # BB_x=60
                        # inverter_x = 18
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        ls=42000

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        BB_x=69
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        ls=82000
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x=(diff*13.5)+69
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    global blockref_inverter;
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    print("len of nedpoint",len(end_point))
                    # blockref_inverter.translate(50000.50,5000.50,100.50)
                    # blockref_inverter.scale(50000.50,50000.50,100.50)
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags,doc, msp)

            else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 20000
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        inv_y = (diff * 26)+75000
                    if MPPTin <= 20:
                        l = 20000
                        ls=26000
                        BB_x = 49
                        inverter_x = 9
                        inv_length = 15000
                        invs_length=16500
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        ls=42000
                        BB_x = 69
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        ls=82000
                        BB_x = 40
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x = (diff * 13.5) + 40
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags, doc,msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': Electricalscheme,
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (1000, 50000)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 200,
            'yscale': 300,
            'rotation': 0
        })
        blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name':GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (1000, 50000)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 500,
                        'yscale': 500,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    # point = (0.62, 0.98)
                    # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.3,
                    #     'rotation': 0
                    # })
                    # point = (0.62, -0.07)
                    # blockref = msp.add_blockref('GND', point, dxfattribs={
                    #     'xscale': 0.5,
                    #     'yscale': 0.5,
                    #     'rotation': 0
                    # })
                    # blockref.add_auto_attribs(ground_name)

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (6200, 2000)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (1000, 50000)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (0.62, -0.07)
                        # blockref = msp.add_blockref('GND', point, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        point = (ns, 2000) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 2000) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # gnd_points = (point[0] + 0.65, 0.98)
                        # blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (ns, -0.07) + dots_list[-1]
                        # gnd_points = (point[0] + 0.65, -0.065)
                        # blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 1500
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 1500
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #
    files = ['BR.dxf','NOTrafoBIc.dxf']
    x = -1
    block_s=['br','trfo']
    # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
    for file in files:
        x+=1
        source_dxf = ezdxf.readfile(file)
        # print(source_dxf.layout_names())
        # for block in source_dxf.blocks:
        #     print("blocksname", block.name)

        if 'A$C20738' not in source_dxf.blocks:
            print("Block  not defined.")
        # print("len", len(source_dxf.blocks))
        # print("blocx",block_s[x])
        # for block in source_dxf.layout_names():
        #     print("blocc", block)
        importer = Importer(source_dxf, doc)
        if block_s[x] =='br' :
            importer.import_block('A$C12511')
        else:
            msp2=source_dxf.modelspace()
            def get_point():
                for text in msp2.query('TEXT'):
                    print("MTEXT content: {}".format(text.dxf.text))
                    if text.dxf.text == '??A':
                        print("hello")
                        print(text.dxf.insert)
                        point = text.dxf.insert
                        return point
            point = get_point()
            importer.import_block('A$C29800')
        importer.finalize()
        # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
        # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
        #     'height': 2000}).set_pos(point, align='CENTER')
        if block_s[x] == 'br':
            block = doc.blocks.get('A$C12511')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name,dots_list[-1])
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            print("*************first tuple",len(dots_list),dots_list[-1][0],type(dots_list[-1]) )
            if dots_list[-1][0] <=50000:
                dots_list[-1]+=((50000-dots_list[-1][0]),0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <=100000):
                dots_list[-1] += ((dots_list[-1][0]//2-30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0]//2-30000), 0)
                # while(dots_list<[-1][0]<= 80000):
                #     x_pos=dots_list[-1][0] // 2
                #
                # dots_list[-1] += ((x_pos - 30000), 0)
            print("*************secnd tuple", len(dots_list), dots_list[-1][0], type(dots_list[-1]))

            block.dxf.base_point = (dots_list[-1])*(-1) + (0,10000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C12511', block.dxf.base_point, dxfattribs={
            'xscale': 3,
            'yscale':2,
          })
            # print("blockrefed",block_refernce.dxf.insert)
        else:
            if maximum <= 20:
                x_l = 13680
                y_l = 1980
                x=61600
                y=16286
            elif maximum > 20 and maximum <= 60:
                x_l = 18520
                y_l = 1980
                x=62571
                y=1286
            elif maximum > 60 and maximum <= 150:
                x_l = 38650
                y_l = 1980
                x=62000
                y=-40000
            print("pointssssss",point,y)
            point_x2=(inv_list[0].dxf.insert[0]+x_l)
            point_y2=(inv_list[0].dxf.insert[1])-y_l
            ls = msp.add_line((-(x+point[0]), (point[1]+y)),(point_x2,point_y2))
            # msp.add_line((-84000, 23000), ls.dxf.end)
            # msp.add_circle(center=(2000, 20000), radius=20000, dxfattribs={'color': 5})
            # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
            #     'height': 2000}).set_pos(point, align='CENTER')
            block = doc.blocks.get('A$C29800')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name)
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            block.base_point = 0
            # ls=msp.add_line(inv_list[0].dxf.insert,(inv_list[0].dxf.insert[0],1000))
            print("maciumum",maximum,block.dxf.base_point)
            if dots_list[0][0] <= 50000:
                dots_list[0] -=((14700 - dots_list[0][0]), 0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[0] -= ((dots_list[0][0] // 2 - 30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0] // 2 - 30000), 0)

            if maximum <= 20 :
                l=4670
                l2=-10200
            elif maximum >20 and maximum <=60:
                l=79100
                l2=-11000
            elif maximum >60 and maximum <=150:
                l=278900
                l2=-13200
            print("dts*******",dots_list)
            print("lengthdd",dots_list[0][0],w_ls,inv_list[0].dxf.insert[0],inv_list[0].dxf.insert[1],(inv_list[0].dxf.insert[0] + w_ls))
            # point_x2 = inv_list[0].dxf.insert[0] -l
            # point_y2 = inv_list[0].dxf.insert[1] - l2
            # # print("88888",point_x2,point_y2)
            point_x2=(dots_list[0][0]) * (-1)
            point_y2=l2

            block.dxf.base_point = (point_x2,-point_y2)
            # block.dxf.base_point = (-50000, 50000, 50000)
            # block.dxf.base_point = (dots_list[-1]) * (-1) + (0, 10000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C29800', block.dxf.base_point, dxfattribs={
                'xscale': 20,
                'yscale': 20,
            })
            # msp.add_line(ls.dxf.start,block_refernce.dxf.insert)

        doc.saveas(dxf_file2_name)


def Ec_brazilian_notrafotri_dxf(res, pvdata, invdata, dxf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        tblock = doc.blocks.new(name='br')
        tblock2 = doc.blocks.new(name='trafo')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        bb_label = doc.blocks.new(name='BB_labels')
        inverter_label = doc.blocks.new(name='inverter_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        DASHED.add_line((4.8, 0.5), (4.9, 0.5))
        DASHED.add_line((5.3, 0.5), (5.4, 0.5))
        DASHED.add_line((5.8, 0.5), (5.9, 0.5))
        DASHED.add_line((6.3, 0.5), (6.4, 0.5))
        DASHED.add_line((6.8, 0.5), (6.9, 0.5))
        DASHED.add_line((7.3, 0.5), (7.4, 0.5))
        DASHED.add_line((7.8, 0.5), (7.9, 0.5))
        DASHED.add_line((8.4, 0.5), (8.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag3 = doc.blocks.new(name='FLAG3')
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_line((0, 1), (0.3, 0.6))
        flag3.add_line((0.5, 1), (0.3, 0.6))
        flag3.add_line((0.25, -0.2), (0.25, 0))
        flag3.add_line((0.25, -0.6), (0.25, -0.5))
        flag3.add_line((0.25, -1), (0.25, -0.9))
        flag3.add_line((0.25, -1.4), (0.25, -1.6))
        flag3.add_line((0.25, -2), (0.25, -2.1))
        flag3.add_line((0.25, -2.4), (0.25, -2.5))
        flag3.add_line((0.25, -2.9), (0.25, -3))
        flag3.add_line((0.25, -3.4), (0.25, -3.5))
        flag3.add_line((0.25, -3.9), (0.25, -4))
        flag3.add_line((0.25, -4.4), (0.25, -4.5))
        flag3.add_line((0.25, -4.9), (0.25, -5))
        flag3.add_line((0.25, 1), (0.25, 1.2))
        flag3.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag3.add_line((-0.5, 1.25), (0.2, 1.25))
        flag3.add_line((-0.5, 1.25), (-0.5, 1))
        flag3.add_line((-0.69, 1), (-0.29, 1))
        flag3.add_line((-0.59, 0.9), (-0.39, 0.9))
        flag3.add_line((-0.56, 0.8), (-0.42, 0.8))
        flag3.add_ellipse((-0.5, 1.12), major_axis=(0.5, 0), ratio=0.09)
        flag3.add_line((-1, 1.12), (-1.6, 1.12))
        flag3.add_line((-1.3, 1.12), (-1.3, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.1, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.5, 1.39))
        flag.add_attdef('NAME', (-2.5, 6.9), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME2', (-2.5, -6), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME3', (-1.2, 2), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME4', (0.35, -0.8), dxfattribs={'height': 0.3, 'color': 3})
        flag3.add_attdef('NAME5', (0.35, -1.2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        DASHED.add_attdef('NAME3', (9, 0.39), dxfattribs={'height': 0.5, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 5), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 3})
        new_labels = doc.blocks.new(name='old_labels')

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 0.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls,maximum,flags
            MPPTin = key
            counts = count_parallelss
            print("maxi",max)
            maximum=max
            flags =False
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    color_list = [1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    # files = ['ModuleNew4.dxf']
                    # x = -1
                    # block_s = ['panels']
                    # # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
                    # for file in files:
                    #     x += 1
                    #     source_dxf = ezdxf.readfile(file)
                    #     # print(source_dxf.layout_names())
                    #     # for block in source_dxf.blocks:
                    #     #     print("blocksname", block.name)
                    #
                    #     if 'A$C20738' not in source_dxf.blocks:
                    #         print("Block  not defined.")
                    #     # print("len", len(source_dxf.blocks))
                    #     # print("blocx", block_s[x])
                    #     # for block in source_dxf.layout_names():
                    #     #     print("blocc", block)
                    #     importer = Importer(source_dxf, doc)
                    #     importer.import_block('module')
                    #     importer.finalize()
                    #     # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
                    #     # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
                    #     #     'height': 2000}).set_pos(point, align='CENTER')
                    #     block = doc.blocks.get('module')
                    #     # print("trarget block name and no of block module", block.name, len(doc.blocks))
                    #     # print("point", point)
                    #     # print("******", block.name)
                    #     block = block.block
                    #     # print(" initial base point beofe panels", block.dxf.base_point)
                    #     base_point = block.dxf.base_point
                    #     block.dxf.base_point = (1600, -48000)
                    #         # block.dxf.base_point = (-50000,-40000, 50000)
                    #     base_point = block.dxf.base_point
                    #     # print(" initial base point panels", block.dxf.base_point)
                    #     block_refernce = msp.add_blockref('module', block.dxf.base_point, dxfattribs={
                    #             'xscale': 10,
                    #             'yscale': 10,
                    #         })
                        # print("blockrefedpanels", block_refernce.dxf.insert)
                    for data in data_raw:
                        # colors += 1
                        # import pdb;
                        # pdb.set_trace()
                        if colors == len(color_list):
                            colors = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 52000
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("dotlist1***8",dots_list[-1])
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5":"Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                flags= True
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 49500
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                colors += 1
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count +=1600
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 800
                            y_count += 1000
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 50000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 300,
                                    'yscale': -200,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    for data in data_raw:
                        temp += 1
                        temp6 += 1

                        if raw == 1:
                            x_count += 1600
                            y_count += 400
                        else:
                            x_count = 780 + point[0]
                            y_count =1 + point[1]
                            x_count += 800
                            y_count += 1000

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 48000)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                dots_list.append(block_s.dxf.insert)
                                print("********",block_s.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 48000)
                                msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 47950)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9600,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1600
                                point = (x_count, 48000)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º' + '\n' + ' 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG3', point, dxfattribs={
                                    'xscale': 200,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 50
                                        point = (x_count, 47950)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9600,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 50
                                    point = (x_count, 47950)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9600,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            if frequency[data] == 1:
                                x_count += 50
                                point = (x_count, 47950)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=20000
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        inv_y = (diff * 26) + 75000
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                    if MPPTin <=20:
                        l = 20000
                        ls=26000
                        # BB_x=48
                        # inverter_x=9
                        inv_length=15000
                        invs_length=16500
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        # BB_x=60
                        # inverter_x = 18
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        ls=42000

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        BB_x=69
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        ls=82000
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x=(diff*13.5)+69
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    global blockref_inverter;
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    print("len of nedpoint",len(end_point))
                    # blockref_inverter.translate(50000.50,5000.50,100.50)
                    # blockref_inverter.scale(50000.50,50000.50,100.50)
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags,doc, msp)

            else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 40000
                        w_l = 10000
                        w_ls = 10000
                        inv_y = 20000
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 50000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=34000
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 105000
                        w_l = 10000
                        w_ls = 10000
                        inv_y=75000
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 100000
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        inv_y = (diff * 26)+75000
                    if MPPTin <= 20:
                        l = 20000
                        ls=26000
                        BB_x = 49
                        inverter_x = 9
                        inv_length = 15000
                        invs_length=16500
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 36000
                        ls=42000
                        BB_x = 69
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3000
                        invs_length=4500
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 76000
                        ls=82000
                        BB_x = 40
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12000
                        invs_length=13500
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 76000
                        ls=(diff*10)+82000
                        BB_x = (diff * 13.5) + 40
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12000
                        invs_length=(diff*4)+13500

                    inverter.add_attdef('inverter_name', (-0.2, 0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, -0.1), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.2), dxfattribs={'height': 0.02, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, 0), dxfattribs={'height': 0.02, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                        if len(end_point) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    else:
                        if len(end_points) <= 3:
                            print("helps", len(end_point), len(end_points))
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': ls,
                                'yscale': 15000,
                                'rotation': 0
                            })
                        else:
                            print("helps2")
                            point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                     list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                            blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                                'xscale': l,
                                'yscale': 15000,
                                'rotation': 0
                            })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,new_labels,flags, doc,msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': Electricalscheme,
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (1000, 50000)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 200,
            'yscale': 300,
            'rotation': 0
        })
        blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name':GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (1000, 50000)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 500,
                        'yscale': 500,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    # point = (0.62, 0.98)
                    # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.3,
                    #     'rotation': 0
                    # })
                    # point = (0.62, -0.07)
                    # blockref = msp.add_blockref('GND', point, dxfattribs={
                    #     'xscale': 0.5,
                    #     'yscale': 0.5,
                    #     'rotation': 0
                    # })
                    # blockref.add_auto_attribs(ground_name)

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (6200, 2000)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (1000, 50000)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (0.62, -0.07)
                        # blockref = msp.add_blockref('GND', point, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        point = (ns, 2000) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 2000) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        # gnd_points = (point[0] + 0.65, 0.98)
                        # blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                        #     'xscale': 0.2,
                        #     'yscale': 0.3,
                        #     'rotation': 0
                        # })
                        # point = (ns, -0.07) + dots_list[-1]
                        # gnd_points = (point[0] + 0.65, -0.065)
                        # blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                        #     'xscale': 0.5,
                        #     'yscale': 0.5,
                        #     'rotation': 0
                        # })
                        # blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 1500
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 1500
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #
    files = ['BR.dxf','NOTrafoTRIc.dxf']
    x = -1
    block_s=['br','trfo']
    # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
    for file in files:
        x+=1
        source_dxf = ezdxf.readfile(file)
        # print(source_dxf.layout_names())
        # for block in source_dxf.blocks:
        #     print("blocksname", block.name)

        if 'A$C20738' not in source_dxf.blocks:
            print("Block  not defined.")
        # print("len", len(source_dxf.blocks))
        # print("blocx",block_s[x])
        # for block in source_dxf.layout_names():
        #     print("blocc", block)
        importer = Importer(source_dxf, doc)
        if block_s[x] =='br' :
            importer.import_block('A$C12511')
        else:
            msp2=source_dxf.modelspace()
            def get_point():
                for text in msp2.query('TEXT'):
                    print("MTEXT content: {}".format(text.dxf.text))
                    if text.dxf.text == '??A':
                        print("hello")
                        print(text.dxf.insert)
                        point = text.dxf.insert
                        return point
            point = get_point()
            importer.import_block('A$C29800')
        importer.finalize()
        # msp.add_circle(center=point, radius=20000, dxfattribs={'color': 5})
        # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
        #     'height': 2000}).set_pos(point, align='CENTER')
        if block_s[x] == 'br':
            block = doc.blocks.get('A$C12511')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name,dots_list[-1])
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            print("*************first tuple",len(dots_list),dots_list[-1][0],type(dots_list[-1]) )
            if dots_list[-1][0] <=50000:
                dots_list[-1]+=((50000-dots_list[-1][0]),0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <=100000):
                dots_list[-1] += ((dots_list[-1][0]//2-30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0]//2-30000), 0)
                # while(dots_list<[-1][0]<= 80000):
                #     x_pos=dots_list[-1][0] // 2
                #
                # dots_list[-1] += ((x_pos - 30000), 0)
            print("*************secnd tuple", len(dots_list), dots_list[-1][0], type(dots_list[-1]))

            block.dxf.base_point = (dots_list[-1])*(-1) + (0,10000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C12511', block.dxf.base_point, dxfattribs={
            'xscale': 3,
            'yscale':2,
          })
            # print("blockrefed",block_refernce.dxf.insert)
        else:
            if maximum <= 20:
                x_l = 13680
                y_l = 1980
                x=61600
                y=16286
            elif maximum > 20 and maximum <= 60:
                x_l = 18520
                y_l = 1980
                x=62571
                y=1286
            elif maximum > 60 and maximum <= 150:
                x_l = 38650
                y_l = 1980
                x=62000
                y=-40000
            print("pointssssss",point,y)
            point_x2=(inv_list[0].dxf.insert[0]+x_l)
            point_y2=(inv_list[0].dxf.insert[1])-y_l
            ls = msp.add_line((-(x+point[0]), (point[1]+y)),(point_x2,point_y2))
            # msp.add_line((-84000, 23000), ls.dxf.end)
            # msp.add_circle(center=(2000, 20000), radius=20000, dxfattribs={'color': 5})
            # msp.add_text("KWXFHHHHHHHHHHHHHHHHHHHHHHHHHHLAST", dxfattribs={
            #     'height': 2000}).set_pos(point, align='CENTER')
            block = doc.blocks.get('A$C29800')
            # print("trarget block name and no of block", block.name, len(doc.blocks))
            # print("point", point)
            # print("******", block.name)
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            block.base_point = 0
            # ls=msp.add_line(inv_list[0].dxf.insert,(inv_list[0].dxf.insert[0],1000))
            print("maciumum",maximum,block.dxf.base_point)
            if dots_list[0][0] <= 50000:
                dots_list[0] -=((14700 - dots_list[0][0]), 0)
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[0] -= ((dots_list[0][0] // 2 - 30000), 0)
            else:
                dots_list[-1] -= ((dots_list[-1][0] // 2 - 30000), 0)

            if maximum <= 20 :
                l=4670
                l2=-10200
            elif maximum >20 and maximum <=60:
                l=79100
                l2=-11000
            elif maximum >60 and maximum <=150:
                l=278900
                l2=-13200
            print("dts*******",dots_list)
            print("lengthdd",dots_list[0][0],w_ls,inv_list[0].dxf.insert[0],inv_list[0].dxf.insert[1],(inv_list[0].dxf.insert[0] + w_ls))
            # point_x2 = inv_list[0].dxf.insert[0] -l
            # point_y2 = inv_list[0].dxf.insert[1] - l2
            # # print("88888",point_x2,point_y2)
            point_x2=(dots_list[0][0]) * (-1)
            point_y2=l2

            block.dxf.base_point = (point_x2,-point_y2)
            # block.dxf.base_point = (-50000, 50000, 50000)
            # block.dxf.base_point = (dots_list[-1]) * (-1) + (0, 10000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            block_refernce = msp.add_blockref('A$C29800', block.dxf.base_point, dxfattribs={
                'xscale': 20,
                'yscale': 20,
            })
            # msp.add_line(ls.dxf.start,block_refernce.dxf.insert)

        doc.saveas(dxf_file2_name)
