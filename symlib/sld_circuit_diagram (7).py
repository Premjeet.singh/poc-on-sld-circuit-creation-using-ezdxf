# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
# import matplotlib
import os
from os import listdir
import sys
import subprocess  # open pdf after plot
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
# from matplotlib.backends.backend_ps import FigureCanvasPS
from sldcircuits.utilities import draw_diagram
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
import ezdxf
from ezdxf.addons import Importer
import random
import os


# matplotlib.use('Agg')  # Set the backend here, to save as PDF
res = {}
res={"String Config":{1: [[9, 9], [10, 10]],

 2: [[9], [10]],

 }}


def Ec(res, pvdata, invdata, svg_file2_name, pdf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.32), (6, 0.32), (6, 0), (0, 0), (0, 0.32)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        rectangle = doc.blocks.new(name='RECTANGLE')
        rectangle.add_lwpolyline([(0, 0.32), (8, 0.32), (8, 0), (0, 0), (0, 0.32)])
        rectangle.add_line((0, 0.01), (8, 0.01))
        rectangle.add_line((0, 0.03), (8, 0.03))
        rectangle.add_line((0, 0.06), (8, 0.06))
        rectangle.add_line((0, 0.09), (8, 0.09))
        rectangle.add_line((0, 0.12), (8, 0.12))
        rectangle.add_line((0, 0.15), (8, 0.15))
        rectangle.add_line((0, 0.18), (8, 0.18))
        rectangle.add_line((0, 0.23), (8, 0.23))
        rectangle.add_line((0, 0.26), (8, 0.26))
        rectangle.add_line((0, 0.29), (8, 0.29))
        rectangle.add_line((0, 0.32), (8, 0.32))
        # rectangle.add_line((0, 0.45), (6, 0.45))

        rectangle.add_line((2.65, 0), (2.65, 0.32))
        KWxh = doc.blocks.new(name='KWxh')
        KWxh.add_circle((0.5, 0.5), radius=0.5)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        points = [(0, 0), (1, 0), (1, 1), (0, 1), (0, 0)]
        KWxh.add_lwpolyline(points)
        KWxh.add_text("KWxh", dxfattribs={
            'style': 'LiberationSerif',
            'color': '1',
            'height': 0.2}).set_pos((0.5, 0.39), align='CENTER')
        Polar = doc.blocks.new(name='Polar')
        Polar.add_arc(center=(6, 2), radius=2.5, start_angle=270, end_angle=90)
        Polar.add_circle(center=(6, 4.5), radius=0.1)
        Polar.add_circle(center=(6, -0.5), radius=0.1)
        Polar.add_line((8, 2), (9, 2))
        bi_Polar = doc.blocks.new(name='Bi_Polar')
        bi_Polar.add_arc(center=(6, 2), radius=2.5, start_angle=270, end_angle=90)
        bi_Polar.add_circle(center=(6, 4.5), radius=0.1)
        bi_Polar.add_circle(center=(6, -0.5), radius=0.1)
        bi_Polar.add_line((8, 2), (9, 2))
        bi_Polar.add_line((8, 1.8), (9, 1.8))
        Tri_Polar = doc.blocks.new(name='Tri_Polar')
        Tri_Polar.add_arc(center=(6, 2), radius=2.5, start_angle=270, end_angle=90)
        Tri_Polar.add_circle(center=(6, 4.5), radius=0.1)
        Tri_Polar.add_circle(center=(6, -0.5), radius=0.1)
        Tri_Polar.add_line((8, 2), (9, 2))
        Tri_Polar.add_line((8, 1.8), (9, 1.8))
        Tri_Polar.add_line((8, 1.6), (9, 1.6))
        string_120 = doc.blocks.new(name='String120')
        string_120.add_line((1, 0), (1.2, 0.5))
        string_120.add_circle(center=(1, 0), radius=0.03,)
        string_120.add_arc(center=(1.05, 0.5), radius=0.02, start_angle=0, end_angle=180)
        RBOX = doc.blocks.new(name='RBOX')
        RBOX.add_lwpolyline([(0, 0.5), (0.2, 0.5), (0.2, 0), (0, 0), (0, 0.5)]),
        RBOX.add_line((-0.2, 0.3), (1.5, 0.5))
        RBOX.add_line((-0.2, 0.3), (-0.2, 0.25))
        RBOX.add_line((0.1, 0), (0.1, -0.2))
        RBOX.add_line((0.02, -0.2), (0.2, -0.2))
        RBOX.add_line((0.05, -0.23), (0.15, -0.23))
        RBOX.add_line((0.08, -0.25), (0.1, -0.25))
        StringBox = doc.blocks.new(name='StringBox')
        StringBox.add_lwpolyline([(0, 8), (12, 8), (12, 0), (0, 0),(0, 8), (12, 8)]),
        StringBox.add_line((9, 5.3), (10, 5.3))
        StringBox.add_line((9, 5.2),(10, 5.2))
        StringBox.add_line((0,0), (12,0))
        StringBox.add_arc((2.9,1.9), radius=0.5, start_angle=180, end_angle=0)
        StringBox.add_arc((1.9,1.9), radius=0.5, start_angle=0, end_angle=180)
        StringBox.add_arc((2.9,1.5), radius=0.5, start_angle=180, end_angle=0)
        StringBox.add_arc((1.9,1.5), radius=0.5, start_angle=0, end_angle=180)
        String_60 = doc.blocks.new(name='String60')
        String_60.add_line((0, 0), (0.5, 0))
        String_60.add_line((0.25, -0.1), (0.25, 0.1))
        String_60.add_line((0.29, -0.1), (0.29, 0.1))
        String_60.add_line((0.33, -0.1), (0.33, 0.1))
        String_60.add_line((0.37, -0.1), (0.37, 0.1))
        String_60.add_line((0.37, 0.1), (0.4, 0.1))
        String_59 = doc.blocks.new(name='String59')
        String_59.add_line((0, 0), (2, 0))
        String_59.add_line((1, 0), (1, 0.1))
        String_59.add_line((1.3, 0), (1.3, 0.1))
        bb_label = doc.blocks.new(name='BB_labels')
        inverter_label = doc.blocks.new(name='inverter_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        # inverter_label.add_attdef('inverter_name', (-13.9, 0.69), dxfattribs={'height': 0.3, 'color': 3})
        # inverter.add_attdef('inverter_names', (1.2, 0.4), dxfattribs={'height': 0.2, 'color': 3})
        # inverter.add_attdef('inverter_max_inputs', (1.2, 0.3), dxfattribs={'height': 0.3, 'color': 3})
        # inverter.add_attdef('inverter_used_inputs', (1.2, -0.2), dxfattribs={'height': 0.3, 'color': 3})
        # inverter.add_attdef('inverter_in_name', (0.59, 0.3), dxfattribs={'height': 0.05, 'color': 3})
        # inverter.add_attdef('inverter_out_name', (0.59, -0.089), dxfattribs={'height': 0.05, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag.add_attdef('NAME', (-1.2, 2.3), dxfattribs={'height': 0.3, 'color': 3})
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 2), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 3})
        KWxh.add_attdef('KWxh_name', (2.5, 0.5), dxfattribs={'height': 0.18, 'color': 3})
        KWxh.add_attdef('KWxh_name2', (2.5, 0), dxfattribs={'height': 0.18, 'color': 3})
        Polar.add_attdef('polar_name1', (22, 4), dxfattribs={'height': 0.9, 'color': 3})
        Polar.add_attdef('polar_name2', (22, 2.6), dxfattribs={'height': 0.9, 'color': 3})
        Polar.add_attdef('polar_name3', (22, 1.2), dxfattribs={'height': 0.9, 'color': 3})
        Polar.add_attdef('polar_name4', (22, -0.2), dxfattribs={'height': 0.9, 'color': 3})
        bi_Polar.add_attdef('polar_name1', (22, 4), dxfattribs={'height': 0.9, 'color': 3})
        bi_Polar.add_attdef('polar_name2', (22, 2.6), dxfattribs={'height': 0.9, 'color': 3})
        bi_Polar.add_attdef('polar_name3', (22, 1.2), dxfattribs={'height': 0.9, 'color': 3})
        bi_Polar.add_attdef('polar_name4', (22, -0.2), dxfattribs={'height': 0.9, 'color': 3})
        Tri_Polar.add_attdef('polar_name1', (22, 4), dxfattribs={'height': 0.9, 'color': 3})
        Tri_Polar.add_attdef('polar_name2', (22, 2.6), dxfattribs={'height': 0.9, 'color': 3})
        Tri_Polar.add_attdef('polar_name3', (22, 1.2), dxfattribs={'height': 0.9, 'color': 3})
        Tri_Polar.add_attdef('polar_name4', (22, -0.2), dxfattribs={'height': 0.9, 'color': 3})
        string_120.add_attdef('string_name1', (2.5, 0.35), dxfattribs={'height': 0.1, 'color': 3})
        string_120.add_attdef('string_name2', (2.5, 0.1), dxfattribs={'height': 0.1, 'color': 3})
        string_120.add_attdef('string_name3', (2.5, -0.1), dxfattribs={'height': 0.1, 'color': 3})
        RBOX.add_attdef('rbox_name1', (2.5, 0.6), dxfattribs={'height': 0.1, 'color': 3})
        RBOX.add_attdef('rbox_name2', (2.5, 0.3), dxfattribs={'height': 0.1, 'color': 3})
        RBOX.add_attdef('rbox_name3', (2.5, 0), dxfattribs={'height': 0.1, 'color': 3})
        StringBox.add_attdef('string_name1', (18, 4), dxfattribs={'height': 0.9, 'color': 3})
        StringBox.add_attdef('string_name2', (18, 2.6), dxfattribs={'height': 0.9, 'color': 3})
        StringBox.add_attdef('string_name3', (18, 1.2), dxfattribs={'height': 0.9, 'color': 3})
        StringBox.add_attdef('string_name4', (18, -0.2), dxfattribs={'height': 0.9, 'color': 3})
        String_60.add_attdef('string_60_name1', (1, 0.9), dxfattribs={'height': 0.05, 'color': 3})
        String_60.add_attdef('string_60_name2', (1, 0.7), dxfattribs={'height': 0.05, 'color': 3})
        String_60.add_attdef('string_60_name3', (1, 0.5), dxfattribs={'height': 0.05, 'color': 3})
        String_60.add_attdef('string_60_name4', (1, 0.3), dxfattribs={'height': 0.05, 'color': 3})
        String_59.add_attdef('string_59_name1', (22, 4), dxfattribs={'height': 0.9, 'color': 3})
        String_59.add_attdef('string_59_name2', (22, 2.6), dxfattribs={'height': 0.9, 'color': 3})
        String_59.add_attdef('string_59_name3', (22, 1.2), dxfattribs={'height': 0.9, 'color': 3})
        String_59.add_attdef('string_59_name4', (22, -0.2), dxfattribs={'height': 0.9, 'color': 3})
        String_59.add_attdef('string_59_name5', (22, 1.2), dxfattribs={'height': 0.9, 'color': 3})


        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 1.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls
            MPPTin = key
            counts = count_parallelss
            maximum=max
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]

                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 0.78 + point[0]
                            y_count = 1 + point[1]
                            x_count += 0.02
                            y_count += 1
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                print("values", values)
                                print("hi")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                print("hello*************")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                # label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                # d.add(e.LABEL, xy=label.end + [1, 0],
                                #       label=str(frequency[data]) + 'string')
                                # # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 4.9,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # # end_stand.append(ENDPV)
                                # if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                #     ENDPV = d.add(e.LINE, d='right',l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     d.add(e.DOT,xy=sf.start+[2.2,0])
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                # if frequency[data] == 1:
                                #     ENDPV = d.add(e.DOT)
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                #     print("gg11111")
                                #     x_count += 0.05
                                #     point = (x_count, -0.06)
                                #     ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                #         'xscale': 3,
                                #         'yscale': -0.2,
                                #         'rotation': 0
                                #     })
                                #     print("circle", ENDPV)

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                # if count_parallel <3 and frequency[data] >3:
                                #     ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     global dots
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # else:
                                #         if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                                #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    print("gg2")
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)

                                else:
                                    if count_parallel != 3 or frequency_count_list[temp7] <= 3 or (
                                            count_parallel != 3 and frequency_count_list[temp7] > 3):
                                        print("hi")

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                print("hih")
                                end_stand.append(ENDPV)
                            print("data freuqsnctlist", frequency[data], data)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                print("gg11111")
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 3,
                                    'yscale': -0.2,
                                    'rotation': 0
                                })
                                print("circle", ENDPV)
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                print("hello")
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                print("hellllo")
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 0.78 + point[0]
                            y_count = 1 + point[1]
                            x_count += 0.02
                            y_count += 1
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        # print("pvumner", pv_number)
                        # print("frkddkddldldllist", frequency_count_list[temp2])
                        # if num != data and count_parallel > 3:
                        #     pv_numbers = pv_number -1
                        # if num != data and count_parallel >= 3 :
                        #     pv_numbers = pv_number - 1
                        #     count_parallel = 1
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                print("values", values)
                                print("hi")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                print("hello*************")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                # label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                # d.add(e.LABEL, xy=label.end + [1, 0],
                                #       label=str(frequency[data]) + 'string')
                                # # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 4.9,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # # end_stand.append(ENDPV)
                                # if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                #     ENDPV = d.add(e.LINE, d='right',l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     d.add(e.DOT,xy=sf.start+[2.2,0])
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                # if frequency[data] == 1:
                                #     ENDPV = d.add(e.DOT)
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                #     print("hi")

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                # if count_parallel <3 and frequency[data] >3:
                                #     ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     global dots
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # else:
                                #         if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                                #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    print("gg2")
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                                else:
                                    if count_parallel != 3 or frequency_count_list[temp7] <= 3 or (
                                            count_parallel != 3 and frequency_count_list[temp7] > 3):
                                        print("hh")

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                print("hhhhh")
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                print("gg11111")
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 3,
                                    'yscale': -0.2,
                                    'rotation': 0
                                })
                                print("circle", ENDPV)
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                print("hello")
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                print("hellos")
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    print("second")
                    for data in data_raw:
                        # import pdb
                        # pdb.set_trace();
                        temp += 1
                        temp6 += 1
                        # doc = ezdxf.new('R2010')
                        #
                        # # Create a block with the name 'FLAG'
                        # flag = doc.blocks.new(name='FLAG')
                        # flag2 = doc.blocks.new(name='FLAG2')
                        # PH = doc.blocks.new(name='PH')
                        # PH.add_line((0, 0), (0.17, 0))
                        # # PH.add_line((0, 0.4), (0, 0.6))
                        # # PH.add_line((0, 0.8), (0, 1))
                        # # PH.add_line((0, 1.2), (0, 1.4))
                        # points = []
                        #
                        # # Add DXF entities to the block 'FLAG'.
                        # # The default base point (= insertion point) of the block is (0, 0).
                        # points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        # flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
                        # flag.add_line((0, 1), (0.3, 0.6))
                        # flag.add_line((0.5, 1), (0.3, 0.6))
                        # flag.add_line((0.25, -0.2), (0.25, 0))
                        # flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
                        # flag2.add_line((0, 1), (0.3, 0.6))
                        # flag2.add_line((0.5, 1), (0.3, 0.6))
                        # flag2.add_line((0.25, -0.2), (0.25, 0))
                        # flag2.add_line((0.25, -0.6), (0.25, -0.5))
                        # flag2.add_line((0.25, -1), (0.25, -0.9))
                        # # flag2.add_line((0.25, -1.4), (0.25, -1.3))
                        # # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
                        # msp = doc.modelspace()

                        # Get 50 random placing points.
                        # placing_points = [get_random_point() for _ in range(50)]
                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 0.78 + point[0]
                            y_count =1 + point[1]
                            x_count += 0.02
                            y_count += 1
                            # x_count += 0.78
                            # y_count += 1 + point[1]
                             # print("points", points)
                        # Every flag has a different scaling and a rotation of -15 deg.
                        random_scale = 0.5 + random.random() * 2.0
                        # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                        # msp.add_blockref('FLAG', point, dxfattribs={
                        #         'xscale': 0.2,
                        #         'yscale': 0.3,
                        #         'rotation': 0
                        #     })
                        # point = (x_count, 0.6)
                        # msp.add_blockref('FLAG2', point, dxfattribs={
                        #         'xscale': 0.2,
                        #         'yscale': 0.3,
                        #         'rotation': 0
                        #     })
                        # x_count += 0.05
                        # point = (x_count, -0.06)
                        # msp.add_blockref('PH', point, dxfattribs={
                        #         'xscale': 5,
                        #         'yscale': 0.1,
                        #         'rotation': 0
                        #     })
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                print("values", values)
                                print("hi")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                print("cordinated",blockref.dxf.insert)
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                                # sf = d.add(e.PV, d='down', label=pv + str(pv_number) + '-' + '1')
                                # dots_list.append(sf)
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, label=pv + str(pv_number) + '-' + str(data))
                            if count_parallel == 3 and frequency[data] > 3:
                                print("hello*************")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME3':  str(frequency[data]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                # label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                # d.add(e.LABEL, xy=label.end + [1, 0],
                                #       label=str(frequency[data]) + 'string')
                                # # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 5,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                                # ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=1.6)
                                # end_stand.append(ENDPV)
                                # x = d.add(e.LINE)
                                # # d.add(e.LINE, xy=sf.start)
                                # # d.add(e.LINE)
                                # d.add(e.DOT, xy=sf.start + [3.9, 0])
                                # sf = d.add(e.PV, d='down', label=pv + str(pv_number + (frequency[data] - 3)) + '-' + '1')
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, label=pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data))
                                if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                    print("hkkkl")
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # end_stand.append(ENDPV)
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)
                                        print("cords1", ENDPV.dxf.insert)
                                        # ENDPV = d.add(e.LINE, d='right', l=2.2)
                                        # end_stand.append(ENDPV)
                                        # # x = d.add(e.LINE)
                                        # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                    # ENDPV = d.add(e.LINE, d='right', l=2.2)
                                    # end_stand.append(ENDPV)
                                    # # x = d.add(e.LINE)
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # if frequency[data] == 1:
                                #     print("gg")
                                #     # ENDPV = d.add(e.DOT)
                                #     # d.add(e.DOT, xy=sf.start + [2.2, 0])
                            # if frequency[data] == 1:
                            #     print("gg11111")
                            #     x_count += 0.05
                            #     point = (x_count, -0.06)
                            #     ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                            #             'xscale': 3,
                            #             'yscale': -0.2,
                            #             'rotation': 0
                            #         })
                            #     print("circle", ENDPV)

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                if count_parallel < 3 and frequency[data] > 3:
                                    print("gg2")
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)
                                    print("cords9", ENDPV.dxf.insert)



                                    # ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=2.2)
                                    # end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    # global dots
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                else:
                                    if count_parallel != 3 or frequency[data] <= 3 or (
                                            count_parallel != 3 and frequency[data] > 3):
                                        print("hh")
                                        # x_count += 0.05
                                        # point = (x_count, -0.06)
                                        # msp.add_blockref('PH', point, dxfattribs={
                                        #     'xscale': 5,
                                        #     'yscale': 0.1,
                                        #     'rotation': 0
                                        # })
                            else:
                                print("hi")
                                # end_stand.append(ENDPV)
                                # sf = d.add(e.PV, d='down', xy=sf.start + [2,0],label='PV' + str(frequency[data]) + '-' + '1')
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, xy=sd.start+[2,0],label='PV' + str(frequency[data]) + '-' + str(data))
                                # ENDPV = d.add(e.LINE, d='right')
                                # d.add(e.SEP)
                                # d.add(e.LINE,xy=sf.start)
                                # d.add(e.SEP)
                                # ENDPV = d.add(e.DOT)
                                # end_stand.append(ENDPV)
                            if frequency[data] == 1:
                                print("gg11111")
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                                print("circle", ENDPV)
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                # import pdb
                # pdb.set_trace();
                if Bat == 2:  # if battery per Building Block do this
                    print("hi")
                    CBout = 1
                    # # d.push()
                    # count = 0
                    # diff = maximum - 150
                    # if diff % 60 == 0:
                    #     diff = int(diff / 60)
                    # else:
                    #     diff = int(diff / 60) + 1
                    # if maximum <= 20:
                    #     n = 6.5
                    #     n_length = 19.3
                    #     m_length = 19.65
                    #     length = 20.9
                    #     # l=30
                    #     ns = 9
                    # elif maximum > 20 and maximum <= 60:
                    #     n = 12
                    #     n_length = 30.2
                    #     m_length = 30.65
                    #     length = 28
                    #     # l=46
                    #     ns = 10
                    # elif maximum > 60 and maximum <= 150:
                    #     n = 18
                    #     n_length = 42.3
                    #     m_length = 42.6
                    #     length = 28
                    #     # l=69
                    #     ns = 18
                    # else:
                    #     n = (diff * 12.5) + 18
                    #     n_length = (25 * diff) + 42.3
                    #     m_length = (25 * diff) + 42.6
                    #     ns = (5 * diff) + 18
                    # if MPPTin <= 20:
                    #     l = 30
                    #     BB_x = 5
                    # elif MPPTin > 20 and MPPTin <= 60:
                    #     l = 48
                    #     BB_x = 7
                    # elif MPPTin > 60 and MPPTin <= 150:
                    #     l = 120
                    #     BB_x = 20.5
                    # else:
                    #     l = (diff * 50) + 120
                    #     BB_x = (diff * 13.5) + 20.5
                    #
                    # for line in end_point:
                    #     count += 1
                    #     if len(data_raw) > 2 and len(data_raw)!=4and len(end_point) == 2:
                    #         # ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    #         # if count % 2 == 0:
                    #         #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                    #         #     x = d.add(e.LINE, d='down', l=3 * l1)
                    #         # else:
                    #         #     if len(end_point) > 1:
                    #         #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                    #         #         d.add(e.LINE, d='down',theta=-90, l=12.6)
                    #         #     else:
                    #         #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                    #         #         d.add(e.LINE, d='down', l=3 * l1)
                    #     if len(data_raw) == 4 and len(end_point) == 2:
                    #         ends = d.add(e.LINE, d='down', xy=line.start, l=1)
                    #         if count % 2 == 0:
                    #             d.add(e.LINE, d='left', xy=ends.end, l=1)
                    #             x = d.add(e.LINE, d='down', l=3 * l1)
                    #         else:
                    #             if len(end_point) > 1:
                    #                 d.add(e.LINE, d='right', xy=ends.end, l=1)
                    #                 x = d.add(e.LINE, d='down', theta=-90, l=n_length)
                    #                 x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1.3, 0],
                    #                                 l=1.8 * l1)
                    #                 d.add(e.LINE, d='down', xy=x_right.end, l=0.18 * l1)
                    #             else:
                    #                 d.add(e.LINE, d='right', xy=ends.end, l=l1)
                    #                 d.add(e.LINE, d='down', l=3 * l1)
                    #
                    #     if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                    #         ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    #     if len(data_raw) >= 3 and len(end_point) ==1:
                    #         ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    #
                    # for line in end_two:
                    #     count += 1
                    #     if len(data_raw) == 2 and len(end_two)!=1:
                    #         ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                    #         if count % 2 == 0:
                    #             d.add(e.LINE, d='right', xy=ends.end, l=1)
                    #             d.add(e.LINE, d='down', l=9.3 * l1)
                    #         else:
                    #             d.add(e.LINE, d='left', xy=ends.end, l=1)
                    #             d.add(e.LINE, d='down', l=3 * l1)
                    #     if len(data_raw) == 2 and len(end_two) == 1:
                    #         ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                    #     if len(data_raw) == 1:
                    #         ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
                    #
                    # d.push()
                    # # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # # d.add(e.CONVDCDC, d='down',
                    # #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    # #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    # #       toplabel='DCDC Converter')
                    # # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # inv_start =d.add(e.LINE, d='down', l=n* l1)
                    # # CBout = 1
                    # # d.add(e.CB, d='down', xy=ENDPV.start,
                    # #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    # #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    # #       toplabel='Combiner Box ' + str(siNb)
                    # #                + '\nInputs: ' + str(Mp)
                    # #                + '\nOutputs: ' + str(CBout))
                    # # d.push()
                    # # d.add(e.LINE, d='left', l=l1)
                    # # d.add(e.CONVDCDC, d='down',
                    # #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    # #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    # #       toplabel='DCDC Converter')
                    # # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # # d.pop()
                    # # d.add(e.LINE, d='down', l=2 * l1)

                else:
                    # import pdb
                    # pdb.set_trace();
                    print("first battt")
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    print("manssn",maximum)
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 10.5
                        w_l = 4
                        w_ls = 4
                        inv_y=6
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 15.5
                        w_l = 7.5
                        w_ls = 4
                        inv_y=14
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 18
                        w_l = 15
                        w_ls = 4
                        inv_y = 30.9
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 18
                        inv_y = (diff * 26) + 30.9
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                    print("MPPVV",MPPTin)
                    if MPPTin <=20:
                        l = 8
                        # BB_x=48
                        # inverter_x=9
                        inv_length=1.5
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 15
                        # BB_x=60
                        # inverter_x = 18
                        inv_length = 3
                        # inv_y=14

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 29
                        BB_x=50
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12
                    else:
                        l = (diff * 10) + 29
                        BB_x=(diff*13.5)+40.5
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12

                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2  and len(data_raw)!=4and len(end_point) == 2:
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #         d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #         x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #             d.add(e.LINE, d='right', xy=ends.end, l=1.5)
                            #             d.add(e.LINE, d='down', theta=-90, l=18.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=1)
                            #         x = d.add(e.LINE, d='down', theta=-90, l=n_length)
                            #         x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1.3, 0],
                            #                         l=1.8*l1)
                            #         d.add(e.LINE, d='down', xy=x_right.end, l=0.18 * l1)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) ==1:
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        count += 1
                        print("hi")
                        # if len(data_raw) == 2 and len(end_two)!=1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                        #     if count % 2 == 0:
                        #         d.add(e.LINE, d='right', xy=ends.end, l=1)
                        #         d.add(e.LINE, d='down', l=m_length)
                        #     else:
                        #         d.add(e.LINE, d='left', xy=ends.end, l=1)
                        #         d.add(e.LINE, d='down', l=3 * l1)
                        # if len(data_raw) ==2 and len(end_two)==1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        # if len(data_raw) == 1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    # d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # inv_start = d.add(e.LINE, d='down', l=n* l1)
                    # point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + 0.2,
                    #          list(end_points.values())[len(end_points) - 1].dxf.insert[1])
                    # line_downss = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                    #     'xscale': 2,
                    #     'yscale': -5,
                    #     'rotation': 0
                    # })
                    print("last", list(end_points.values())[len(end_points) - 1].dxf.insert[1])
                    # global blockref_inverter;
                    # point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - 0.8,
                    #          list(end_points.values())[len(end_points) - 1].dxf.insert[1] - 6)
                    print("points****************8", point)
                    inverter.add_attdef('inverter_name', (-0.2, 0.4), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0.3), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, 0), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.3), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, -0.089), dxfattribs={'height': 0.09, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs + '10',
                        'inverter_used_inputs': UsedInputs + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    # point = (0.35, 0.9)
                    # point_labels = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - 0.8,
                    #          list(end_points.values())[len(end_points) - 1].dxf.insert[1] - 6)
                    # labels = msp.add_blockref('inverter_Labels', point_labels, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.3,
                    #     'rotation': 0
                    # })
                    # labels.add_auto_attribs(values)
                    global blockref_inverter;
                    point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                             list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                    blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                        'xscale': l,
                        'yscale': 1.5,
                        'rotation': 0
                    })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp, msp)
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                # INVDCAC = e.update_inverter(l)
                # inv= d.add(INVDCAC, d='down',
                #       toplabel=InverterNr  + str(siNb) +
                #                '\n'+MaxInputs+': ' + str(Nb_Mppt)
                #                + '\n'+UsedInputs+': ' + str(MPPTin))
                # d.add(e.LABEL, xy=inv.end + [BB_x, 2], smlltlabel=In + str(VL2) + V + '/ ' + str(AL2) + A)
                # d.add(e.LABEL, xy=inv.end + [BB_x, 0.5], smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A)
                # inv_list.append(inv)
                # draw_diagram(end_points, data_raw, frequency, counts, end_point, inv, inv_start, count_list,
                #                        MPPTin, maximum, diff,total_mpptin,total_dict_length,Mp,d)

            else:
                # import pdb
                # pdb.set_trace();
                if Bat == 2:  # if battery per BB do this
                    print("hisecond b2")
                    # CBout = 1
                    # direction = 'right'
                    # count = 0
                    # diff = maximum - 150
                    # if diff % 60 == 0:
                    #     diff = int(diff / 60)
                    # else:
                    #     diff = int(diff / 60) + 1
                    # if maximum <= 20:
                    #     n = 6.5
                    #     n_length = 19.3
                    #     m_length = 19.65
                    #     length = 20.9
                    #     # l=30
                    #     ns = 9
                    # elif maximum > 20 and maximum <= 60:
                    #     n = 12
                    #     n_length = 30.2
                    #     m_length = 30.65
                    #     length = 28
                    #     # l=46
                    #     ns = 10
                    # elif maximum > 60 and maximum <= 150:
                    #     n = 18
                    #     n_length = 42.3
                    #     m_length = 42.6
                    #     length = 28
                    #     # l=69
                    #     ns = 18
                    # else:
                    #     n = (diff * 12.5) + 18
                    #     n_length = (25 * diff) + 42.3
                    #     m_length = (25 * diff) + 42.6
                    #     ns = (5 * diff) + 18
                    # if MPPTin <= 20:
                    #     l = 30
                    #     BB_x = 5
                    # elif MPPTin > 20 and MPPTin <= 60:
                    #     l = 48
                    #     BB_x = 7
                    # elif MPPTin > 60 and MPPTin <= 150:
                    #     l = 120
                    #     BB_x = 20.5
                    # else:
                    #     l = (diff * 50) + 120
                    #     BB_x = (diff * 13.5) + 20.5
                    #
                    # for line in end_point:
                    #     count += 1
                    #     if len(data_raw) > 2 and len(data_raw)!=4 and len(end_point) == 2:
                    #         ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    #         # if count % 2 == 0:
                    #         #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                    #         #     x = d.add(e.LINE, d='down', l=3 * l1)
                    #         # else:
                    #         #     if len(end_point) > 1:
                    #         #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                    #         #         d.add(e.LINE, d='down', theta=-90, l=12.6)
                    #         #     else:
                    #         #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                    #         #         d.add(e.LINE, d='down', l=3 * l1)
                    #     if len(data_raw) == 4 and len(end_point) == 2:
                    #         ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                    #         if count % 2 == 0:
                    #             d.add(e.LINE, d='left', xy=ends.end, l=1)
                    #             x = d.add(e.LINE, d='down', l=3 * l1)
                    #         else:
                    #             if len(end_point) > 1:
                    #                 d.add(e.LINE, d='right', xy=ends.end, l=1)
                    #                 x = d.add(e.LINE, d='down', theta=-90, l=n_length)
                    #                 x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1.3, 0],
                    #                                 l=1.8 * l1)
                    #                 d.add(e.LINE, d='down', xy=x_right.end, l=0.18 * l1)
                    #             else:
                    #                 d.add(e.LINE, d='right', xy=ends.end, l=l1)
                    #                 d.add(e.LINE, d='down', l=3 * l1)
                    #
                    #     if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                    #         ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    #     if len(data_raw) >= 3 and len(end_point) ==1 :
                    #         ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    # for line in end_two:
                    #     count += 1
                    #     if len(data_raw) == 2 and len(end_two)!=1:
                    #         ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                    #         if count % 2 == 0:
                    #             d.add(e.LINE, d='right', xy=ends.end, l=1)
                    #             d.add(e.LINE, d='down', l=m_length)
                    #         else:
                    #             d.add(e.LINE, d='left', xy=ends.end, l=1)
                    #             d.add(e.LINE, d='down', l=3 * l1)
                    #     if len(data_raw) == 2 and len(end_two) == 1:
                    #         ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                    #     if len(data_raw) == 1:
                    #         ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
                    #
                    # d.push()
                    # # d.add(e.LINE, d='right', l=2 * l1)
                    # # d.add(e.CONVDCDC, d='down',
                    # #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    # #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    # #       toplabel='DCDC Converter')
                    # # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # inv_start = d.add(e.LINE, d='down', l=n * l1)
                    # # global inv;
                    # INVDCAC = e.update_inverter(l)
                    # inv = d.add(INVDCAC,
                    #       toplabel=InverterNr  + str(siNb) +
                    #                '\n'+MaxInputs+': ' + str(Nb_Mppt)
                    #                + '\n'+UsedInputs+': ' + str(MPPTin))
                    # d.add(e.LABEL, xy=inv.end + [BB_x, 2], smlltlabel=In + str(VL2) + V + '/ ' + str(AL2) + A)
                    # d.add(e.LABEL, xy=inv.end + [BB_x, 0.5], smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A)
                    # inv_list.append(inv)
                    #
                    # draw_diagram(end_points, data_raw, frequency, counts, end_point, inv, inv_start,
                    #                        count_list, MPPTin, maximum,total_mpptin,total_dict_length, d)

                else:  # if NO battery  do this
                    # import pdb
                    # pdb.set_trace();
                    print("**********************No")
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    print("manssn", maximum)
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 10.5
                        w_l = 4
                        w_ls = 4
                        inv_y=6
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 15.5
                        w_l = 7.5
                        w_ls = 4
                        inv_y=14
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 25.5
                        w_l = 4
                        w_ls = 10.5
                        inv_y=30.9
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 25.5
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        inv_y = (diff * 26)+30.9
                    print("MPPVV", MPPTin)
                    if MPPTin <= 20:
                        l = 8
                        BB_x = 48
                        inverter_x = 9
                        inv_length = 1.5
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 15
                        BB_x = 70
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 29
                        BB_x = 40.5
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 29
                        BB_x = (diff * 13.5) + 40.5
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12

                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2 and len(data_raw) != 4 and len(end_point) == 2:
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #         d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #         x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #             d.add(e.LINE, d='right', xy=ends.end, l=1.5)
                            #             d.add(e.LINE, d='down', theta=-90, l=18.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=1)
                            #         x = d.add(e.LINE, d='down', theta=-90, l=n_length)
                            #         x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1.3, 0],
                            #                         l=1.8*l1)
                            #         d.add(e.LINE, d='down', xy=x_right.end, l=0.18 * l1)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) == 1:
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        count += 1
                        print("hi")
                        # if len(data_raw) == 2 and len(end_two)!=1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                        #     if count % 2 == 0:
                        #         d.add(e.LINE, d='right', xy=ends.end, l=1)
                        #         d.add(e.LINE, d='down', l=m_length)
                        #     else:
                        #         d.add(e.LINE, d='left', xy=ends.end, l=1)
                        #         d.add(e.LINE, d='down', l=3 * l1)
                        # if len(data_raw) ==2 and len(end_two)==1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        # if len(data_raw) == 1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    # d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # inv_start = d.add(e.LINE, d='down', l=n* l1)
                    # point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + 0.2,
                    #          list(end_points.values())[len(end_points) - 1].dxf.insert[1])
                    # line_downss = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                    #     'xscale': 2,
                    #     'yscale': -5,
                    #     'rotation': 0
                    # })
                    print("last",
                          list(end_points.values())[len(end_points) - 1].dxf.insert[1])
                    # global blockref_inverter;
                    # point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - 0.8,
                    #          list(end_points.values())[len(end_points) - 1].dxf.insert[1] - 6)
                    print("points****************8", point)
                    inverter.add_attdef('inverter_name', (-0.2, 0.4), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_max_inputs', (-0.2, 0.3), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_used_inputs', (-0.2, 0), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_in_name', (1, 0.3), dxfattribs={'height': 0.09, 'color': 3})
                    inverter.add_attdef('inverter_out_name', (1, -0.089), dxfattribs={'height': 0.09, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs + '10',
                        'inverter_used_inputs': UsedInputs + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    # point = (0.35, 0.9)
                    # point_labels = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - 0.8,
                    #                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - 6)
                    # labels = msp.add_blockref('inverter_Labels', point_labels, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.3,
                    #     'rotation': 0
                    # })
                    # labels.add_auto_attribs(values)
                    # global blockref_inverter;
                    point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                             list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                    blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                        'xscale': l,
                        'yscale': 1.5,
                        'rotation': 0
                    })
                    blockref_inverter.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp, msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    # ENDBB = d.add(e.GND, botlabel=GND, l=l1)
                    # d.add(e.DOT)
                    # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel=BB + str(1))
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    # project_name = {
                    #     'projectname': "ELECTRICAL CIRCUIT",
                    #     'XPOS': 1,
                    #     'YPOS': 0.5
                    # }
                    point = (0.35, 0.9)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    point = (0.62, 0.98)
                    blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    point = (0.62, -0.07)
                    blockref = msp.add_blockref('GND', point, dxfattribs={
                        'xscale': 0.5,
                        'yscale': 0.5,
                        'rotation': 0
                    })

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                    # BB1DOT = d.add(e.DOT)
                else:
                    # ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * ns, 0],
                    #               botlabel=GND, l=l1)
                    # d.add(e.DOT, xy=ENDBB.start)
                    # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                    #       titlabel=BB + str(no))
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)
                    # d.add(e.LINE, xy=inv.end, to=BB1DOT.start - [0, 0], l=l1)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        # ENDBB = d.add(e.GND, botlabel=GND, l=l1)
                        # d.add(e.DOT)
                        # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel=BB + str(1))
                        point = (0.62, 0.98)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (0.5, 0.98)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        point = (0.62, -0.07)
                        blockref = msp.add_blockref('GND', point, dxfattribs={
                            'xscale': 0.5,
                            'yscale': 0.5,
                            'rotation': 0
                        })
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        # ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * ns, 0],
                        #               botlabel=GND, l=l1)
                        # d.add(e.DOT, xy=ENDBB.start)
                        # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                        #       titlabel=BB + str(no))
                        print("nsnsss",ns)
                        point = (ns, 0.98) + dots_list[-1]
                        print("dotslist", dots_list[-1])
                        print("pinddndnd", point[0])
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 0.98) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        gnd_points = (point[0] + 0.65, 0.98)
                        blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        point = (ns, -0.07) + dots_list[-1]
                        gnd_points = (point[0] + 0.65, -0.065)
                        blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                            'xscale': 0.5,
                            'yscale': 0.5,
                            'rotation': 0
                        })

                        print("**************w_ldkddkdk*************88",w_l)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 0.15
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 0.15
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
                        # 3.3
                        # 3.25
                        # 4.3
                        # d.add(e.LINE, xy=inv.end, to=inv_list[0].end, l=l1)
        # # -----------------------------------------------------------------------------
        #
    print("dotslistt",dots_list)
    pv_name = {
            'pvname': PVmodule,
            'XPOS': 1.3,
            'YPOS': 0.2
        }
    point = (ns, 0.6)+ dots_list[-1]
    block_s = msp.add_blockref('PV', point, dxfattribs={
            'xscale': 0.2,
            'yscale': 0.3,
            'rotation': 0
        })
    block_s.add_auto_attribs(pv_name)
    cable_name = {
            'cable_name': cabletype,
            'XPOS': 5,
            'YPOS': 0.2
        }
    combinerbox_name = {
        'combinerboxname': CombinerBox,
        'XPOS': 1.3,
        'YPOS': 0.2
    }
    point = (ns, 0.6) + dots_list[-1]
    point = (ns, 0.35) + dots_list[-1]
    block_ss = msp.add_blockref('COMBINERBOX', point, dxfattribs={
            'xscale': 0.2,
            'yscale': 0.3,
            'rotation': 0
        })
    block_ss.add_auto_attribs(combinerbox_name)
    inverter_name = {
            'inverter_names': InverterDCACtype,
            'XPOS': 1.3,
            'YPOS': 0.2
        }
    point = (ns, 0.22) + dots_list[-1]
    inverter_blocks = msp.add_blockref('INVERTER', point, dxfattribs={
            'xscale': 0.3,
            'yscale': 0.3,
            'rotation': 0
        })



    doc.saveas("blockref_read_replicate2.dxf")
        # # The Combiner Box after the Building Blocks
        #
        # if len(res_data) == len(values[0].keys()):
        #     if NumBB > 1:
        #         d.add(e.CB, d='down', xy=inv_list[0].end,
        #               smlltlabel=In + str(VL3) + V + ' / ' + str(AL3) + A,
        #               smlrtlabel=Out + str(VL3) + ' V / ' + str(AL3 * NumBB) + A,
        #               toplabel=CombinerBoxAllBB
        #                        + '\n' + Inputs + ': ' + str(NumBB)
        #                        + '\n' + Outputs + ': 1')

        # -----------------------------------------------------------------------------

        # If the system uses only one Battery, it gets added here:

        # if Bat == 1:
        #     d.push()
        #     d.add(e.LINE, d='left', l=2*l1)
        #     d.add(e.INVACDC, d='down',
        #           smllblabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
        #           smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
        #           toplabel='Inverter transformer')
        #     d.add(e.BATTERY, d='down', toplabel='Battery All BB')
        #     d.pop()
        #     d.add(e.LINE, d='down', l=1*l1)

        # -----------------------------------------------------------------------------

        # The connection to the electrical grid:
        # if NumBB == 1:
        #     Grid = d.add(e.GRID, d='down',xy=inv.end, toplabel=gridconnection)
        # else:
        #     Grid = d.add(e.GRID, d='down', toplabel=gridconnection)

        # -----------------------------------------------------------------------------

        # Finishing the drawing, saving it to a pdf and automatically open it.

# Array4SLD = {"String Config": {
#         # 2:[[8,8],[19],[20],[29],[30],[31],[32]],
#         # 3: [[14, 14,14,14],[14],[14],[15,5],[19,19,19]],
#     #     4:[[9,9],[8,8],[10,10],[20]],
#     5: [[9, 9], [8, 8], [10, 10], [20],[11, 11], [12, 12], [13, 13], [14],[15, 15], [16, 16], [17, 17], [18],[19,19],[21, 21], [1, 1], [2, 2], [3, 3], [5, 5], [6],[7, 7],[22, 22], [23, 23], [24],[25, 25],[26, 26], [27, 27], [28, 28], [29],[30, 30], [31, 31], [32, 32], [33],[34, 34], [35, 35], [36, 36], [37],[38,38],[39, 39], [40,40], [41,41], [42,42], [43], [44],[45,45],[46,46], [47,47], [48],[49,49],[10, 10], [20],[11, 11], [12, 12], [13, 13], [14],[15, 15], [16, 16], [17, 17], [18],[19,19],[21, 21], [1, 1], [2, 2], [3, 3], [5, 5], [6],[7, 7],[22, 22], [23, 23], [24],[25, 25],[26, 26], [27, 27], [28, 28], [29],[30, 30], [31, 31], [32, 32], [33],[34, 34], [35, 35], [36, 36], [37],[38,38],[39, 39], [40,40], [41,41], [42,42], [43], [44],[45,45],[46,46], [47,47], [48],[49,49]],
#     6:[[9,9],[8],[5,5,5]],
#     7: [[9, 9], [8, 8], [10, 10], [20], [9, 9], [8, 8], [10, 10], [20], [9, 9], [8, 8],
#         [10, 10], [20], [9, 9], [8, 8], [10, 10], [20], [9, 9], [8, 8], [10, 10]],
#     8: [[9],[10]],

#     }}
#
# PVData = {
#     'I_mp_ref': 39,
#     'V_mp_ref': 7,
# }
# INVData = {
#
# }
# print(Ec(Array4SLD,PVData,INVData,"en"))
