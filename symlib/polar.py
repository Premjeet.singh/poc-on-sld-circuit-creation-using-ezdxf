from typing import List
import entity


class POLAR(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.Line(entity.Point(8, 2), entity.Point(9, 2)),
            entity.Arc.from_crse(center=entity.Point(6, 2), radius=2.5, start=270, end=90),
            entity.Circle(center=entity.Point(6, 4.5), radius=0.1),
            entity.Circle(center=entity.Point(6, -0.5), radius=0.1)
        ]


class BiPOLAR(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.Line(entity.Point(8, 2), entity.Point(9, 2)),
            entity.Line(entity.Point(8, 1.8), entity.Point(9, 1.8)),
            entity.Arc.from_crse(center=entity.Point(6, 2), radius=2.5, start=270, end=90),
            entity.Circle(center=entity.Point(6, 4.5), radius=0.1),
            entity.Circle(center=entity.Point(6, -0.5), radius=0.1)
        ]


class TriPOLAR(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.Line(entity.Point(8, 2), entity.Point(9, 2)),
            entity.Line(entity.Point(8, 1.8), entity.Point(9, 1.8)),
            entity.Line(entity.Point(8, 1.6), entity.Point(9, 1.6)),
            entity.Arc.from_crse(center=entity.Point(6, 2), radius=2.5, start=270, end=90),
            entity.Circle(center=entity.Point(6, 4.5), radius=0.1),
            entity.Circle(center=entity.Point(6, -0.5), radius=0.1)
        ]