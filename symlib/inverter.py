from typing import List
import entity


class INVERTER(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.PolyLine(
                points=[entity.Point(0, 8), entity.Point(12, 8), entity.Point(12, 0), entity.Point(0, 0), entity.Point(0, 8),entity.Point(12, 0)],
                closed=True),
            entity.Line(entity.Point(9, 6), entity.Point(10, 6)),
            entity.Line(entity.Point(9, 5.8), entity.Point(10, 5.8)),
            entity.Arc.from_crse(center=entity.Point(2, 2), radius=0.5, start=0, end=180),
            entity.Arc.from_crse(center=entity.Point(3, 2), radius=0.5, start=180, end=0),
            # entity.Circle(center=entity.Point(10, 0), radius=10)
        ]


class SEPARATOR(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.Line(entity.Point(2, 0), entity.Point(2.2, 0.1)),
            entity.Line(entity.Point(2.1, 0), entity.Point(2.3, 0.1)),
            # entity.Circle(center=entity.Point(10, 0), radius=10)
        ]


class CABLE(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            # entity.Line(entity.Point(0, 2), entity.Point(5, 2),entity.Point(5, 5)),
            entity.PolyLine(
                points=[entity.Point(0, 8), entity.Point(12, 8)],
                closed=True),
            # entity.Circle(center=entity.Point(10, 0), radius=10)
        ]


class GRIDCONNECTION(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.PolyLine(
                points=[entity.Point(0, 5), entity.Point(6, 5), entity.Point(6, 0), entity.Point(0, 0), entity.Point(0, 5)],
                closed=True),
            entity.Line(entity.Point(2.5, 0), entity.Point(2.5, 5)),
            # entity.Circle(center=entity.Point(10, 0), radius=10)
        ]


class STRINGBOX(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.PolyLine(
                points=[entity.Point(0, 8), entity.Point(12, 8), entity.Point(12, 0), entity.Point(0, 0), entity.Point(0, 8)],
                closed=True),
            entity.Arc.from_crse(center=entity.Point(5.9, 3.9), radius=0.5, start=180, end=0),
            entity.Arc.from_crse(center=entity.Point(4.9, 3.9), radius=0.5, start=0, end=180),
            entity.Arc.from_crse(center=entity.Point(5.9, 3.5), radius=0.5, start=180, end=0),
            entity.Arc.from_crse(center=entity.Point(4.9, 3.5), radius=0.5, start=0, end=180),
            # entity.Circle(center=entity.Point(10, 0), radius=10)
        ]

class GND(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.Line(entity.Point(0, 2), entity.Point(0, 2.1)),
            entity.Line(entity.Point(-0.08, 2), entity.Point(0.08, 2)),
            entity.Line(entity.Point(-0.05, 1.99), entity.Point(0.05, 1.99)),
            entity.Line(entity.Point(-0.03, 1.98), entity.Point(0.03, 1.98)),
            # entity.PolyLine(
            #     points=[entity.Point(0, 8), entity.Point(12, 8)],
            #     closed=True),
            # entity.Circle(center=entity.Point(10, 0), radius=10)
        ]


class INVERSOR(entity.CodedSymbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate(self):
        return [
            entity.PolyLine(
                points=[entity.Point(0, 8), entity.Point(12, 8), entity.Point(12, 0), entity.Point(0, 0),entity.Point(0, 8), entity.Point(12, 8)],
                closed=True),
            entity.Line(entity.Point(9, 3), entity.Point(10, 3)),
            entity.Line(entity.Point(9, 2.9), entity.Point(10, 2.9)),
            # entity.Arc.from_crse(center=entity.Point(2, 2), radius=0.5, start=0, end=180),
            # entity.Arc.from_crse(center=entity.Point(3, 2), radius=0.5, start=180, end=0)
            entity.Arc.from_crse(center=entity.Point(2.9, 5.9), radius=0.5, start=180, end=0),
            entity.Arc.from_crse(center=entity.Point(1.9, 5.9), radius=0.5, start=0, end=180),
            entity.Arc.from_crse(center=entity.Point(2.9, 5.5), radius=0.5, start=180, end=0),
            entity.Arc.from_crse(center=entity.Point(1.9, 5.5), radius=0.5, start=0, end=180),
            # entity.Circle(center=entity.Point(10, 0), radius=10)
        ]
