#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 10:35:22 2021

@author: vipluv.aga
"""

import matplotlib.pyplot as plt
from sldcircuitsdxf.sld_circuit_diagram import Ec_dxf
from sldcircuit.PVScheme.sld_circuit_diagram import Ec,Ec_brazilian

class SLDMaker():
    def __init__(self, res, PVData, INVData):
        # self.array4sld = array4sld.get('StringArrayConfig')
        self.res = res
        self.PVData = PVData
        self.INVData = INVData

    def makebraziliansld(self, bucket, svg_file2save, pdf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
        Ec_brazilian(self.res, self.PVData, self.INVData, svg_file2save, pdf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt)
        success = "SLD brazilian generated"
        return success

    def makesld(self, bucket, svg_file2save, pdf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
        Ec(self.res, self.PVData, self.INVData, svg_file2save, pdf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt)
        success = "SLD generated"
        return success

    def makedxfsld(self, bucket, dxf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
        Ec_dxf(self.res, self.PVData, self.INVData, dxf_file2save, ProjName, PvName, InvName, lang, Wp, paco,
           Nb_Mppt)
        success = "SLD generated"
        return success
    